#include "CString.h"
#include <assert.h>

///////////////////////////////////////////////////////////////////////////////
// Custom allocator for CString. Allocates the specified number of extra bytes before the string

class CFormatStringAllocatorExtra: public CFormatStringAllocator
{
public:
	CFormatStringAllocatorExtra( int extra ) { m_Extra=extra; }
	virtual bool Realloc( void *&ptr, int size );

private:
	int m_Extra;
};

bool CFormatStringAllocatorExtra::Realloc( void *&ptr, int size )
{
	void *ptr2=ptr?(char*)ptr-m_Extra:NULL;
	if (!CFormatStringAllocator::Realloc(ptr2,size+m_Extra)) {
		ptr=NULL;
		return false;
	}
	ptr=(char*)ptr2+m_Extra;
	return true;
}

///////////////////////////////////////////////////////////////////////////////
// CStringA - ANSI implementation

void CStringA::Clear( void )
{
	if (m_pText) {
		long ref=InterlockedDecrement(&GetRef());
		assert(ref>=0);
		if (ref==0)
			free(&GetRef());
	}
	m_pText=NULL;
}

void CStringA::Set( const char *text, int len )
{
	// the 2 strings must not overlap
	assert(!text || !m_pText || text+Strlen(text)<m_pText || m_pText+Strlen(m_pText)<text);
	Clear();
	if (text) {
		if (len==-1) len=Strlen(text);
		m_pText=(char *)malloc(len+1+sizeof(long));
		if (!m_pText)
			return;
		(char*&)m_pText+=sizeof(long);
		memcpy(m_pText,text,len);
		m_pText[len]=0;
		GetRef()=1;
	}
	else
		m_pText=NULL;
}

CStringA& CStringA::Format( const char *format, FORMAT_STRING_ARGS_CPP )
{
	Clear();
	CFormatStringAllocatorExtra allocator(sizeof(long));
	m_pText=FormatStringAlloc(allocator,format,FORMAT_STRING_ARGS_PASS);
	GetRef()=1;
	return *this;
}

///////////////////////////////////////////////////////////////////////////////
// CStringW - UNICODE implementation

void CStringW::Clear( void )
{
	if (m_pText) {
		long ref=InterlockedDecrement(&GetRef());
		assert(ref>=0);
		if (ref==0)
			free(&GetRef());
	}
	m_pText=NULL;
}

void CStringW::Set( const wchar_t *text, int len )
{
	// the 2 strings must not overlap
	assert(!text || !m_pText || text+Strlen(text)<m_pText || m_pText+Strlen(m_pText)<text);
	Clear();
	if (text) {
		if (len==-1) len=Strlen(text);
		m_pText=(wchar_t *)malloc(len*2+2+sizeof(long));
		if (!m_pText)
			return;
		(char*&)m_pText+=sizeof(long);
		memcpy(m_pText,text,len*2);
		m_pText[len]=0;
		GetRef()=1;
	}
	else
		m_pText=NULL;
}

CStringW& CStringW::Format( const wchar_t *format, FORMAT_STRING_ARGS_CPP )
{
	Clear();
	CFormatStringAllocatorExtra allocator(sizeof(long));
	m_pText=FormatStringAlloc(allocator,format,FORMAT_STRING_ARGS_PASS);
	GetRef()=1;
	return *this;
}
