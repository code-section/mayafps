#ifndef _CSTRING_H
#define _CSTRING_H

// CStringA, CStringW - simple string containers
// They use reference counting to avoid having multiple copies of the same string
// in memory.
//
///////////////////////////////////////////////////////////////////////////////

#include "StringUtils.h"
#include "FormatString.h"
#include <windows.h>

///////////////////////////////////////////////////////////////////////////////
// ANSI version

class CStringA
{
public:
	CStringA( void )
	{
		m_pText=NULL;
	}

	CStringA( const CStringA &s )
	{
		m_pText=s.m_pText;
		if (m_pText) InterlockedIncrement(&GetRef());
	}

	void operator=( const CStringA &s )
	{
		Clear();
		m_pText=s.m_pText;
		if (m_pText) InterlockedIncrement(&GetRef());
	}

	CStringA& operator = (const char* text ) { Set( text ); return *this; }
	CStringA& operator += (const char* text) { CStringA temp = *this; Format( "{0}{1}", temp, text ); return *this; }

	explicit CStringA( const char *text, int len=-1 )
	{
		m_pText=NULL;
		Set(text,len);
	}

	~CStringA( void )
	{
		Clear();
	}

	void Clear( void );
	void Set( const char *text, int len=-1 );
	CStringA& Format( const char *format, FORMAT_STRING_ARGS_H );
	const char* CStr( void ) const { return m_pText?m_pText:""; }
	bool Empty() const { return m_pText == 0 || m_pText[0] == 0; }

	operator const char *( void ) const
	{
		return CStr();
	}

	operator CFormatArg( void ) const
	{
		return CFormatArg(m_pText);
	}

private:
	char *m_pText;

	long &GetRef( void ) { return ((long*)m_pText)[-1]; }
};

///////////////////////////////////////////////////////////////////////////////
// UNICODE version

class CStringW
{
public:
	CStringW( void )
	{
		m_pText=NULL;
	}

	CStringW( const CStringW &s )
	{
		m_pText=s.m_pText;
		if (m_pText) InterlockedIncrement(&GetRef());
	}

	void operator=( const CStringW &s )
	{
		Clear();
		m_pText=s.m_pText;
		if (m_pText) InterlockedIncrement(&GetRef());
	}
	CStringW& operator = (const wchar_t* text ) { Set( text ); return *this; }
	CStringW& operator += (const wchar_t* text) { CStringW temp = *this; Format( L"{0}{1}", temp, text ); return *this; }

	explicit CStringW( const wchar_t *text, int len=-1 )
	{
		m_pText=NULL;
		Set(text,len);
	}

	~CStringW( void )
	{
		Clear();
	}

	void Clear( void );
	void Set( const wchar_t *text, int len=-1 );
	CStringW& Format( const wchar_t *format, FORMAT_STRING_ARGS_H );
	const wchar_t* CStr( void ) const { return m_pText?m_pText:L""; }
	bool Empty() const { return m_pText == 0 || m_pText[0] == 0; }

	operator const wchar_t *( void ) const
	{
		return CStr();
	}

	operator CFormatArg( void ) const
	{
		return CFormatArg(m_pText);
	}

private:
	wchar_t *m_pText;

	long &GetRef( void ) { return ((long*)m_pText)[-1]; }
};

///////////////////////////////////////////////////////////////////////////////

#ifdef _UNICODE
typedef CStringW CString;
#else
typedef CStringA CString;
#endif


inline CStringA Strconv( const wchar_t* text, int len=-1 )
{
	if( text == 0 || text[0] == 0 )
		return CStringA();
	if( len < 0 ) len = Strlen( text );
	if( len == 0 )
		return CStringA();
	char* s = new char[ len + 1 ];
	WcsToMbs( s, len+1, text );
	CStringA str( s, len );
	delete [] s;
	return str;
}

inline CStringW Strconv( const char* text, int len=-1 )
{
	if( text == 0 || text[0] == 0 )
		return CStringW();
	if( len < 0 ) len = Strlen( text );
	if( len == 0 )
		return CStringW();
	wchar_t* s = new wchar_t[ len + 1 ];
	MbsToWcs( s, len+1, text );
	CStringW str( s, len );
	delete [] s;
	return str;
}
#endif
