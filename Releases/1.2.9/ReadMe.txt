MayaFPS 1.2.9 (1.3.beta)

For more information about this update, please visit http://code-section.com/mayafps/update
or visit the plugin's homepage, or contact the author for support.

http://code-section.com/mayafps