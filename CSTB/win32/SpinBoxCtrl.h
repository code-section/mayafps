// This is a custom spin control which implements a more intuitive and faster user
// interaction method (click and drag instead of click and hold). Also, it doesn't store position
// and range information - it simply provides notifications that allow the parent to implement
// easy number modification. Same goes for buddy pairing and acceleration stuff.
// -Adel Amro - http://code-section.com

# ifndef CSTB_SPINBOX_H
# define CSTB_SPINBOX_H


# include <windows.h>


# define WC_A3DSPINBOX		TEXT( "A3DSpinBox" )


BOOL A3DSpinBox_Init( HINSTANCE ); ///<
VOID A3DSpinBox_Uninit(); ///<



/// Structure sent with notification messages to the parent.
struct NMSPINBOX
{
	NMHDR hdr;
	INT deltaValue; ///< The amount by which the value should be changed.
	//DWORD mouseButtons; ///< Use MK_LBUTTON and its sisters to check which mouse buttons are down.
	WPARAM wParam; ///< The wParam parameter of mouse-related messages. Used to check virtual key states.
	DWORD additionalInfo;
};



/// Spinbox control notifications. These are sent to the parent window using the WM_NOTIFY message.
# define SBN_DRAGSTART		1
# define SBN_DELTAVALUE		2 ///< The user has done something to change the corresponding value. See NMSPINBOX.deltaValue.
# define SBN_VALUECHANGE	2 ///< Same as SBN_DELTAVALUE
# define SBN_DRAGEND		3
# define SBN_DRAGCANCEL		4 ///< The user has pressed escape (or clicked a different mouse button) while dragging.

/** @name Supported Common Notifications:
* NM_LDOWN - Sent when the control receives a WM_LBUTTONDOWN message. Return nonzero to prevent further processing.
*/


/// This constant is used with the SBN_DELTAVALUE notification messages. If the additionalInfo
/// member of the notification struct is set to SB_FIRST_CHANGE, then this is the first delta value
/// notification message sent. This is useful as it allows the application to backup the current
/// value controlled by the spin box control for undo.
# define SB_FIRST_CHANGE ((DWORD)1)


/// Constants identifying the different parts of the control. Used with SpinBox_DoHitTest().
# define SBP_NONE		-1
# define SBP_DRAG		1
# define SBP_INCREMENT	2
# define SBP_DECREMENT	3

/// Layout constants. The control automatically adjusts the layout based on window dimensions.
# define SBLAYOUT_HORIZONTAL	1 ///< Button parts are arranged side by side.
# define SBLAYOUT_VERTICAL		2 ///< Button parts are arranged on top of each other (stack).


VOID SpinBox_GetRects( HWND hSpinBox, RECT* prcIncrement, RECT* prcDecrement, RECT* prcDrag );
INT SpinBox_GetLayout( HWND hSpinBox );
BOOL SpinBox_IsDragging( HWND hSpinBox );
VOID SpinBox_EndDragging( HWND hSpinBox, BOOL bNotify ); ///< Can be used to cancel an ongoing dragging operation.

/// Sets the window to which the control will send notification messages. This is automatically set to the control's
/// parent when the control is created.
VOID SpinBox_SetNotifyWnd( HWND hSpinBox, HWND hNewNotify );
HWND SpinBox_GetNotifyWnd( HWND hSpinBox ); ///<

BOOL SpinBox_IsDragOnly( HWND hSpinBox ); ///< Whether or not the control shows the side arrows.
VOID SpinBox_SetDragOnly( HWND hSpinBox, BOOL bDragOnly ); ///<

UINT SpinBox_GetDragPixels( HWND hSpinBox ); ///<
VOID SpinBox_SetDragPixels( HWND hSpinBox, UINT dragPixels ); ///< The "step size" in pixels.

//BOOL SpinBox_ShowPart( HWND hSpinBox, INT part, BOOL bShow );
//BOOL SpinBox_EnablePart( HWND hSpinBox, INT part, BOOL bEnable );



# endif // include guard