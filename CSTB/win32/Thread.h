
/*
Win32 thread wrapper. Derive from the Thread class and override the virtual ThreadMain() as follows:
while( !ShouldDie() )
{
	stuff
}
*/

# ifndef CSTB_THREAD_H
# define CSTB_THREAD_H


# include <windows.h>



namespace CSTB
{



class Thread
{
	//This function is used only to call the virtual function (which must be overridden).
	static DWORD WINAPI ThreadMain(LPVOID lParam)
	{
		Thread *pThis = static_cast<Thread*>(lParam);
		if(pThis != NULL)
			return pThis->ThreadMain( pThis->m_arg );
		else return -1;
	}

private:
	LPARAM		m_arg;

protected:
	DWORD		m_dwThreadID;
	HWND		m_hMainWnd;
	HANDLE		m_hThread;
	HANDLE		m_hStopEvent;

	//! Checks if the thread should abort the action
	BOOL ShouldDie() { return(::WaitForSingleObject(m_hStopEvent, 0) == WAIT_OBJECT_0); };

	// virtual main thread function
	// This function must be overridden when new class is derived from this one.
	virtual UINT ThreadMain( LPARAM lParam ) = 0;

public:
	Thread()
	{
		m_dwThreadID = 0;
		m_hMainWnd = NULL;
		m_hThread = NULL;
		m_hStopEvent = NULL;
	}
	virtual ~Thread() { StopThread(); }



	// Thread start function
	// Use this function to start the thread execution.
	BOOL StartThread(HWND hWnd = NULL, LPARAM arg = 0 )
	{
		// if thread already running do not start another one
		if(GetThreadStatus())
			return FALSE;

		// create stop event
		m_hStopEvent = ::CreateEvent(NULL, TRUE, FALSE, NULL);
		if(m_hStopEvent == NULL)
			return FALSE;

		// create thread
		DWORD dwThreadId;
		m_arg = arg;
		m_hThread = ::CreateThread(NULL, 0, ThreadMain, this, 0, &dwThreadId);
		if(m_hThread == NULL)
			return FALSE;

		m_dwThreadID = dwThreadId;

		return TRUE;
	}





	// Thread termination function
	// Use this function to stop the thread execution. This signals the internal event.
	// The thread's main function is supposed to be checking the event continuously and
	// yield when it sees it's been set, but if it's not for some reason, the timeout
	// parameter is used to stop the thread eitherway.
	BOOL StopThread( DWORD timeout = INFINITE )
	{
		// signal stop event and close all threads
		if(::SetEvent(m_hStopEvent))
		{
			// if the Thread Main function is not overridden correctly
			// this will freeze the application
			::WaitForSingleObject(m_hThread, timeout );
			::CloseHandle(m_hThread);
			::CloseHandle(m_hStopEvent);
			return TRUE;
		}

		return FALSE;
	}




	// Returns thread running status
	// Use this function to get the thread status. It returns true if the thread
	// is running and false if not.
	BOOL GetThreadStatus()
	{
		// check is the thread still active
		DWORD dwExitCode = 0;
		GetExitCodeThread(m_hThread, &dwExitCode);
		return(dwExitCode == STILL_ACTIVE);
	}
};



}; // namespace CSTB


# endif // include guard