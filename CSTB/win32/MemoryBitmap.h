// Win32 memory bitmap wrapper.


# pragma once
# ifndef MEMORY_BITMAP_H
# define MEMORY_BITMAP_H



# include <windows.h>



namespace CSTB
{


class MemoryBitmap
{
public:
	HBITMAP			hbmp = 0;
	BITMAPINFO		bmi = {0};
	BYTE*			pixels = 0;
	INT width = 0, height = 0, bpp = 0, rowSize = 0;


	~MemoryBitmap() { if( hbmp ) DeleteObject( hbmp ); }

	BOOL Create( UINT _width, UINT _height, UINT _bpp = 32 );
	void Cleanup() { if( hbmp ) { DeleteObject( hbmp ); hbmp = 0; pixels = 0; width = height = 0; } }

	void Clear()
	{
		BYTE* p = pixels;
		for( INT y=0; y<height; y++ )
		{
			ZeroMemory( p, rowSize );
			p += rowSize;
		}
		
	}

	void SetPixel24( UINT x, UINT y, BYTE r, BYTE g, BYTE b )
	{
		UINT yFinal = y;
		BYTE* p = pixels + rowSize * yFinal + x * 3;
		p[0] = r; p[1] = g; p[2] = b;
	}

	void SetPixel32( UINT x, UINT y, BYTE a, BYTE r, BYTE g, BYTE b )
	{
		UINT yFinal = y;
		DWORD* p = (DWORD*)pixels;
		p[ x + yFinal * width ] = a << 24 | r << 16 | g << 8 | b;
	}

	void SetPixel32( UINT x, UINT y, DWORD color )
	{
		DWORD* p = (DWORD*)pixels;
		p[ x + y * width ] = color;
	}

	DWORD GetPixel32( UINT x, UINT y ) const
	{
		DWORD* p = (DWORD*)pixels;
		return p[ x + y * width ];
	}
	

	// Parameters are similar to the Windows SDK's BitBlt().
	BOOL Blit( HDC hDC, INT xDest, INT yDest, INT width, INT height, INT xSrc, INT ySrc, DWORD dwRop );
	BOOL Blit( HDC hDC ) { return Blit( hDC, 0, 0, width, height, 0, 0, SRCCOPY ); }

	BOOL Load( LPCTSTR sFile );

	// Save a bitmap to a .bmp file.
	BOOL Save( const PBITMAPINFO pbmi, LPCTSTR sBmpFileName, void* pPixels );
};

}; // namespace CSTB

# endif // include guard