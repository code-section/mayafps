/**
Header file for the Window class, which is basically just a wrapper for Win32 windows,
with some convenient virtual methods to define window behavior easily.
Preprocessor configuration options:
define CSTB_WINDOW_USE_EVENTS to define a public eventMsgProc.
define CSTB_WINDOW_USE_MAP to attach C++ object to HWND using a map instead of GWLP_USERDATA.

Copyright Adel Amro - http://code-section.com
*/

# ifndef CSTB_WINDOW_H
# define CSTB_WINDOW_H


# include <windows.h>


//# define CSTB_WINDOW_USE_EVENTS

# ifdef CSTB_WINDOW_USE_EVENTS
# include <CSTB/Event.h>
# endif




# define CSTB_WINDOWCLASS TEXT("CodeSectionWindow")


namespace CSTB
{


/// This is the base window class that other windows can derive from. An instance of this class is attached
/// to the system's corresponding HWND by storing a pointer in the window's user data (GWL_USERDATA).
/// An instance of this window can be attached to an existing window to subclass it, or it can be used to
/// create a new window (in which case the window class name passed to the Create() method should be CSTB_WINDOWCLASS).
///
class Window
{
public:
	HWND		hWnd;
	//HMENU		hMenu;
	HACCEL		m_hAccel;



# ifdef WINDOW_USE_EVENTS
	static DECLARE_EVENT( LRESULT, Window& wnd, UINT Msg, WPARAM wParam, LPARAM lParam, BOOL& bHandled ) eventMsgProc;
# endif


	Window();
	virtual ~Window(); ///< If there is a window (HWND) attached to this object, it will be destroyed.


	/// Creates a window and associates it with this object. className can be either WC_A3DWINDOW, or the name
	/// of a registered window class like "EDIT".
	BOOL Create( DWORD styleEx, LPCTSTR className, LPCTSTR title, DWORD style,
				int x, int y, int width, int height, HWND parent, HMENU menu, HINSTANCE hInst, void* lParam );


	/// Sets the window's accelerator to the specified one. The window object then owns the accelerator and it
	/// will destroy it when it is destroyed or when SetAccelerator() is called again with a different accelerator.
	VOID SetAccelerator( HACCEL hNewAccel )
	{
		if( m_hAccel == hNewAccel )
			return;
		if( m_hAccel )
			DestroyAcceleratorTable( m_hAccel );
		m_hAccel = hNewAccel;
	}

	/*void SetMenu( HMENU hNewMenu )
	{
		if( hMenu )
			DestroyMenu( hMenu );
		hMenu = hNewMenu;
		::SetMenu( hWnd, hMenu );
	}*/

	/// Detaches this object from the associated HWND, if any, and restores the window's original window proc
	/// if the window was originally subclassed by this class.
	virtual BOOL Detach();


	/// This attaches this class instance to the specified window and also replaces its
	/// window proc with the instance's window proc.
	/// Fails if this instance is already assigned an HWND or the passed HWND has an instance
	/// of this class assigned to it. See the Subclass() method of ADialog to see why this method is virtual.
	virtual BOOL Subclass( HWND hNewWnd );


	/// Gets the handle of the window associated with this object.
	HWND GetHandle() { return hWnd; }

	/// HWND conversion operator.
	operator HWND () const { return hWnd; }


	/// If this object has subclassed a window, this calls the window's original proc.
	/// Otherwise, it calls DefWindowProc().
	virtual LRESULT DefWndProc( UINT Msg, WPARAM wParam, LPARAM lParam )
	{
		if( m_OriginalProc )
			return CallWindowProc( m_OriginalProc, hWnd, Msg, wParam, lParam );

		return DefWindowProc( hWnd, Msg, wParam, lParam );
	}


	/// The window's message processor. The default implementation responds to some messages by calling
	/// the appropriate OnXXX() methods and calls DefWndProc() if the message is not processed.
	virtual LRESULT MsgProc( UINT Msg, WPARAM wParam, LPARAM lParam );



	/// Return 1 if the window translates the message.
	/// Return -1 if you do not wish to allow parent window to translate the message.
	/// Return 0 to allow parent to translate message.
	virtual int TranslateMessage( MSG* pMsg )
	{
		if( m_hAccel )
			return ::TranslateAccelerator( hWnd, m_hAccel, pMsg );
		return 0;
	}


	/// Called in responce to a WM_COMMAND message. hCtrl is a handle to the control sending the message.
	/// id is the identifier of the control, and the meaning of 'code' depends on the control.
	/// hCtrl is WM_COMMAND's LPARAM. id is LOWORD(wParam), and code is HIWORD(wParam).
	/// Return TRUE if the command is handled, FALSE otherwise.
	virtual LRESULT OnCommand( HWND hCtrl, WORD id, WORD code ) { return 0; }

	/// Called in response to WM_NOTIFY.
	/// wParam is the identifier of the control sending the notification, but use pNMH->idFrom to check instead.
	/// Set bHandled to TRUE to use the return value, otherwise the return value of DefWndProc() is used.
	virtual LRESULT OnNotify( WPARAM wParam, NMHDR* pNMH, BOOL& bHandled ) { return 0; }



	/// Retrieves a pointer to an Window class associated with the specified HWND, if any.
	static Window* FromHandle( HWND hWnd );


	/// If wait is true then the function will wait until a message arrives.
	/// This checks the messages for all windows in the app.
	/// The function returns FALSE when a WM_QUIT message is received.
	/// Set hWnd to 0 (default) to check messages for all windows.
	static BOOL CheckMessages( bool wait, HWND hWnd = 0 );

	VOID ShowModal() {
		SetCloseAction( CLOSE_DESTROY );
		while( Window::CheckMessages( true, hWnd ) && hWnd )
			;
	}


	LPVOID	GetUserData() const { return m_pUserData; }
	VOID	SetUserData( LPVOID pUserData ) { m_pUserData = pUserData; }


	enum CloseAction
	{
		CLOSE_DEFAULT,
		CLOSE_QUIT,
		CLOSE_DESTROY,
		CLOSE_HIDE
	};
	void SetCloseAction( CloseAction ca ) { m_closeAction = ca; }
	void QuitOnClose() { SetCloseAction( CLOSE_QUIT ); }

protected:

	CloseAction m_closeAction;


	/// @name Mousing
	/// @{
	/// MsgProc() calls this to arrange for the mouse event methods to be called. The default mousing
	/// behavior is to capture the mouse when a button is pressed and release capture if all buttons are
	/// released. To suppress this behavior, for example if you are doing mouse processing
	/// from within MsgProc() and don't need the mousing methods to be called, then override this
	/// to just {return 0;}
	virtual LRESULT	ProcessMouse( UINT Msg, WPARAM wParam, LPARAM lParam, BOOL& bHandled );

	/// A mouse button has been pressed. Return TRUE if you want to prevent the message from being
	/// passed to the default message processor, or FALSE otherwise.
	virtual BOOL	OnMousePress( UINT Msg, WPARAM wParam, POINT ptCursor ) { return FALSE; }

	/// A mouse button has been released. Return TRUE if you want to prevent the message from being
	/// passed to the default message processor, or FALSE otherwise.
	virtual BOOL	OnMouseRelease( UINT Msg, WPARAM wParam, POINT ptCursor ) { return FALSE; }

	/// The mouse capture has been lost. This can be due to the user releasing all mouse buttons, or
	/// due to "external" reasons, like the application being forced to the background.
	/// Return TRUE if you want to prevent the message from being
	/// passed to the default message processor, or FALSE otherwise.
	virtual BOOL	OnMouseCaptureLost( HWND hNewCaptureWnd ) { return FALSE; }

	/// Called when the mouse moves over the window. Inspect the wParam parameter to determine what
	/// mouse buttons and keys are being pressed, if any (see MK_LBUTTON etc.).
	/// Return TRUE if you want to prevent the message from being
	/// passed to the default message processor, or FALSE otherwise.
	virtual BOOL	OnMouseMove( WPARAM wParam, POINT ptCursor, POINT ptPrevCursor ) { return FALSE; }

	virtual BOOL	OnMouseHover( POINT ptCursor, WPARAM wParam ) { return FALSE; }
	virtual BOOL	OnMouseLeave() { return FALSE; }

	/// @}



	/// Called when the window is created (during Window::Create(), in response to WM_CREATE).
	/// Return TRUE to allow the creation of the window, false to prevent it (fail).
	virtual BOOL OnCreate( CREATESTRUCT* pCS ) { return TRUE; }

	/// Called in response to WM_CLOSE. Return TRUE for default processing, FALSE to prevent further processing.
	virtual BOOL OnClose()
	{
		switch( m_closeAction ) {
			case CLOSE_QUIT: PostQuitMessage( 0 ); return FALSE;
			case CLOSE_DESTROY: DestroyWindow( hWnd ); return FALSE;
			case CLOSE_HIDE: ShowWindow( hWnd, SW_HIDE ); return FALSE;
		}
		return TRUE;
	}

	/// Called in response to WM_DESTROY, when the associated win32 is about to be destroyed.
	/// This can not be used for destroying the associated Window class instance.
	/// Use OnNCDestroy() instead.
	virtual VOID OnDestroy() { }

	/// This method is called in response to WM_NCDESTROY. It's the last method that is called by the system
	/// and can therefore be used to destroy the Window class instance associated with the native window
	/// being destroyed. The window handle hWnd is NULL by the time this method is called.
	virtual VOID OnNCDestroy() { }

	/// This is called when the window is resized (WM_SIZE).
	virtual VOID OnSize() { }


	/// Attaches the Window object to the passed HWND so that Window::CheckMessages() will dispatch
	/// window messages to the MsgProc() method of this Window object. This is used internally in the
	/// Create() method and the Subclass() method.
	virtual BOOL Attach( HWND hNewWnd );

	/// Hooks a function that waits to attach this Window object to the next window created.
	int EnableCreationHook();


	WNDPROC		m_OriginalProc;
	LPVOID		m_pUserData;

private:
	POINT m_ptPrevCursor;

	static LRESULT CALLBACK CreationHookProcedure( int nCode, WPARAM wParam, LPARAM lParam );
	static BOOL RegisterClass( HINSTANCE hInstance );

	// Just marshals each message to its respective window class.
	static LRESULT CALLBACK StaticMsgProc( HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam );
};


}; // namespace CSTB


# endif // inclusion guard