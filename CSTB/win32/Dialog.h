/**
Dialog class for win32 dialogs. Can be used to build dialogs in-memory.

Copyright Adel Amro - http://code-section.com
**/

# ifndef CSTB_DIALOG_H
# define CSTB_DIALOG_H


# include "Window.h"
# include "DialogTemplate.h"


namespace CSTB
{


class Dialog : public Window
{
public:

	BOOL Create( HINSTANCE hInstance, HWND hParent, LPCTSTR templateName ); // Modeless.
	INT_PTR DoModal( HINSTANCE hInstance, HWND hParent, LPCTSTR templateName ); // Modal.


	BOOL CreateIndirect( HINSTANCE hInstance, HWND hParent, LPCDLGTEMPLATE pDialogTemplate ); // Modeless.
	INT_PTR DoModalIndirect( HINSTANCE hInstance, HWND hParent, LPCDLGTEMPLATE pDialogTemplate ); // Modal.


	// Controls should be added in the WM_INITDIALOG handler.
	BOOL CreateEmpty( HINSTANCE hInstance, HWND hParent, LPCTSTR sCaption, DWORD style, DWORD styleEx,
		INT x, INT y, INT cx, INT cy, LPCTSTR sFontFace = NULL, WORD wFontSize = 8 )
	{
		DialogTemplate dt;
		dt.Create( sCaption, style, styleEx, x, y, cx, cy, sFontFace, wFontSize );
		return CreateIndirect( hInstance, hParent, dt );
	}

	BOOL Subclass( HWND wnd );

protected:
	// Return FALSE if the message is not handled, TRUE otherwise.
	LRESULT DefWndProc( UINT Msg, WPARAM wParam, LPARAM lParam )
	{
		if( m_OriginalProc )
			CallWindowProc( m_OriginalProc, hWnd, Msg, wParam, lParam );
		return 0; // Let default dialog procedure handle the message.
	}

	virtual int TranslateMessage( MSG* pMsg );


	/// Message processor. The default implementation calls some of the OnEVENT() methods below.
	virtual LRESULT MsgProc( UINT Msg, WPARAM wParam, LPARAM lParam );

	/// Called in response to WM_INITDIALOG. Initialize dialog controls with data here.
	virtual BOOL OnInitDialog() { return TRUE; }

	virtual LRESULT OnCommand( HWND hCtrl, WORD id, WORD code );

private:
	static INT_PTR CALLBACK DlgProc( HWND, UINT, WPARAM, LPARAM );
};


}; // namespace CSTB


# endif // include guard