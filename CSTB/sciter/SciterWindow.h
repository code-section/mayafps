# ifndef CSTB_SCITER_WINDOW_H
# define CSTB_SCITER_WINDOW_H

# include <CSTB\Win32\Window.h>
# include <tchar.h>
# pragma warning( push )
# pragma warning( disable: 4995 )
# include <sciter-x.h>
# pragma warning( pop )




namespace sciter
{
/// Utility function to update native variables from sciter objects. Usage: sciter::GetValue( val["prop1"], refNativeVar );
template<typename T>
T GetValue( const value_key_a& vk, T& t ) { t = ((value)vk).get( t ); return t;}

inline float GetValue( const value_key_a& vk, float& t )
{ double d=(double)t; return t = (float)GetValue( vk, d ); }


/// This utility function can be used to exchange data between script and native variables. Doing the exchange
/// using one call reduces code size and possibility of string typing error. Usage:
/// GetSetValue( bDirection, val["key"], refNativeVar );
template<typename T>
inline void ExchangeValue( bool get, value_key_a& vk, T& t )
{ if( get ) GetValue( vk, t ); else vk = t; }

};




namespace CSTB
{


class SciterWindow : public Window, public sciter::event_handler
{
public:

	/// Creates the window and sets up the callback which calls the OnSciterNotify() method.
	BOOL Create( DWORD dwStyleEx, LPCTSTR sTitle, DWORD dwStyle,
		int x, int y, int width, int height, HWND hParentWnd,
		HMENU hMenu, HINSTANCE hInstance, void* lParam );

	virtual BOOL LoadFile( LPCTSTR sPath, BOOL bMakeAbsPath = TRUE );
	virtual BOOL Reload() { return m_sFileName.empty() ? FALSE : LoadFile( m_sFileName.c_str(), FALSE ); }


	/// Passes messages to SciterProcND() then to AWindow::DefWindowProc().
	virtual LRESULT MsgProc( UINT Msg, WPARAM wParam, LPARAM lParam );



	/// While there are numerous notifications that we can handle from sciter (mouse and keybaord
	/// events for example), handling only script calls enforces a client-server-like paradigm,
	/// where sciter and tiscript are like a web browser, and the native app is like a server.
	virtual bool on_script_call(HELEMENT he, LPCSTR name, UINT argc, SCITER_VALUE* argv, SCITER_VALUE& retval)
	{ return CallNativeFunction( name, he, argc, argv, retval ); }



	/// Handles notifications from sciter.
	virtual LRESULT OnSciterNotify( LPSCITER_CALLBACK_NOTIFICATION pn );

	virtual LRESULT OnAttachBehavior( LPSCN_ATTACH_BEHAVIOR pn );




	/// @{
	/// Native functions that can be called from script

	typedef bool(*PFNativeFunction)( HWND hWnd, HELEMENT he, LPCSTR name, UINT argc, SCITER_VALUE* argv, SCITER_VALUE& retval );

	static void				RegisterNativeFunction( PFNativeFunction, const char* name );
	static void				RemoveNativeFunction( const char* name );
	static PFNativeFunction	GetNativeFunction( const char* name );

	bool CallNativeFunction( const char* name, HELEMENT he, UINT argc, SCITER_VALUE* argv, SCITER_VALUE& retval )
	{
		PFNativeFunction pf = GetNativeFunction( name );
		if( pf ) return pf( hWnd, he, name, argc, argv, retval );
		return false;
	}
	/// @}

	/// Call script function that is defined in tiscript code loaded in this window.
	BOOL Call( LPCSTR functionName, UINT argc, const SCITER_VALUE* argv, SCITER_VALUE& retval )
	{ return SciterCall( hWnd, functionName, argc, argv, &retval ); }


	
	/// Inside script code, script can call
	/// Subscribe( "eventName", function( [args] ){ code } );
	/// to subscribe to an application-defined event. The app can then trigger the event using this method.
	/// Script code needs to call Unsubscribe();
	static bool TriggerEvent( const char* name, int argc, const sciter::value* argv, sciter::value* ret = nullptr );
	static inline bool TriggerEvent( const char* name, sciter::value* ret = nullptr ) { return TriggerEvent( name, 0, 0, ret ); }
	static inline bool TriggerEvent( const char* name, const sciter::value& args, sciter::value* ret = nullptr )
	{ return TriggerEvent( name, 1, &args, ret ); }


protected:
	static UINT CALLBACK NotifyHandler( LPSCITER_CALLBACK_NOTIFICATION pNotification, LPVOID lParam )
	{
		SciterWindow* pWnd = (SciterWindow*)lParam;
		if( pWnd ) return pWnd->OnSciterNotify( pNotification );
		return 0;
	}

	sciter::string m_sFileName;

};


}; // namespace CSTB



# endif