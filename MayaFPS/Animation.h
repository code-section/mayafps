# ifndef MAYAFPS_ANIM_H
# define MAYAFPS_ANIM_H

# include "MayaFPS.h"
# include <CSTB/win32/Window.h>
# include <Maya/MFnCamera.h>
# include <Maya/MDagPath.h>
# include <Maya/MFnTransform.h>
# include <Maya/MFnAnimCurve.h>
# include <Maya/MAnimControl.h>
# include <Maya/MGlobal.h>
# include <Maya/MPlug.h>
# include <Maya/MTime.h>
# include "ResString.h"
# include <vector>



struct Anim
{
	struct Snapshot
	{
		MVector translation, rotation;
		DWORD time;
	};

	bool bRecording = false;
	MObject objCameraTransformNode;
	MObject objTranslationAttr;
	MObject objRotationAttr;
	std::vector< Snapshot > snapshots;
	bool bCommandQueued = false;
	MTime startTime;
	const char* sCommand = "mayafpsanim";
	static void* CreateAnimCommand();
};

extern Anim anim;

/*
namespace Anim
{
	struct Snapshot
	{
		MVector translation;
		MVector rotation;
		//MVector worldScalePivot, worldRotatePivot;
		DWORD time;
	};

	bool bRecording = false;


	GLOBALVAR_INIT( BOOL, bRecording, FALSE );
	GLOBALVAR( MObject, objCameraTransformNode );
	GLOBALVAR( MObject, objTranslationAttr );
	GLOBALVAR( MObject, objRotationAttr );

	GLOBALVAR( A3D::AVectorT< Snapshot >,  snapshots );

	GLOBALVAR_INIT( BOOL, bCommandQueued, FALSE );
	GLOBALVAR( MTime, startTime ); // The time when recording was started.

	GLOBALVAR_INIT( const char*, sCommand, "mayafpsanim" );
	void* CreateAnimCommand();
};
*/


# endif // inclusion guard
