# include "MayaFPS.h"
# include "settings.h"
# include <CSTB/win32/Dialog.h>
# include <CSTB/win32/Win32Util.h>
# include "ResString.h"
# include "Globals.h"
# include <windowsx.h>
# include <shellapi.h>


//# pragma comment( lib, "version.lib" )



class CAboutDialog : public CSTB::Dialog
{
public:

	BOOL OnInitDialog()
	{
		CString sAbout;
		sAbout.Format( GET_STRING( IDS_ABOUT_FORMAT ),
			ProductName,
			ProductVersion,
			TEXT(""),
			GET_STRING( IDS_PRODUCT_URL ));
		//INT len = Strlen( sAbout );
		//SendMessage( GetDlgItem( hWnd, IDC_ABOUT ), EM_SETLIMITTEXT, len, 0 );
		SetWindowText( GetDlgItem( hWnd, IDC_ABOUT ), sAbout.CStr() );

		// Update control states.
		Button_SetCheck( GetDlgItem( hWnd, IDC_AUTO_UPDATE ),
			settings.general.autoUpdate ? BST_CHECKED : BST_UNCHECKED );
		
		return TRUE;
	}




	LRESULT OnCommand( HWND hCtrl, WORD id, WORD code )
	{
		switch( id )
		{
		case IDC_AUTO_UPDATE:
			settings.general.autoUpdate = (BST_CHECKED == Button_GetCheck(hCtrl));
			return 0;

		case IDC_VISIT_WEBSITE:
			ShellExecute( hWnd, TEXT("open"), GET_STRING(IDS_PRODUCT_URL), NULL, NULL, SW_SHOWNORMAL );
			return 0;

		case IDC_CHECK_UPDATES:
			//MessageBox( hWnd, TEXT("le message box" ), TEXT(" unload plugin now" ), MB_OK );
			CheckUpdates( TRUE, GetDlgItem( hWnd, IDC_CHECK_UPDATES ) );
			return 0;

		case IDOK:
			// fall through
		case IDCANCEL:
			DestroyWindow( hWnd );
			hWnd = NULL;
			return 0;
		}
		return Dialog::OnCommand( hCtrl, id, code );
	}

} g_aboutDialog;




BOOL ShowAboutDialog()
{
	if( g_aboutDialog.hWnd )
	{
		//cout << __FUNCTION__": Already shown" << endl;
		SetActiveWindow( g_aboutDialog.hWnd );
		return FALSE;
	}
	if( globals.hInstance == NULL )
	{
		cout << GET_STRING( IDS_ERR_INVALID_HMODULE ) << endl;
		return FALSE;
	}
	return g_aboutDialog.Create( globals.hInstance, M3dView::applicationShell(), MAKEINTRESOURCE(IDD_ABOUT) );
}
