# ifndef RES_STRING_H
# define RES_STRING_H


# include <Windows.h>
# include <FormatString\CString.h>
# include "resource.h"
//# include "Globals.h"


CStringA& LoadString( UINT id, CStringA& s );
CStringW& LoadString( UINT id, CStringW& s );


# ifndef GET_STRING
# define GET_STRING_A(id)	LoadString( id, CStringA())
# define GET_STRING(id)		LoadString( id, CString() )
# endif


# endif