
# include "Animation.h"
# include <Maya/MPxCommand.h>
# include <Maya/MDGModifier.h>
# include <Maya/MPlugArray.h>




class CAnimCommand : public MPxCommand
{
public:
	CAnimCommand() {}
	virtual ~CAnimCommand() { }

	MStatus		doIt( const MArgList& );
	MStatus		redoIt();
	MStatus		undoIt();
	bool		isUndoable() const { return true; }

protected:
	MObject		m_objCameraTransformNode;
	MObject		m_objTranslationAttr;
	MObject		m_objRotationAttr;
	MDGModifier m_DGModifier;
};




MStatus DisconnectIncoming( MPlug& plug, MDGModifier& dgModifier )
{
	MPlugArray plugs;
	plug.connectedTo( plugs, true, false );
	if( plugs.length() == 0 )
		return MStatus::kSuccess;

	for( unsigned i=0; i<plugs.length(); i++ )
		dgModifier.disconnect( plugs[i], plug );
	dgModifier.doIt();

	return MStatus::kSuccess;
}




MStatus CAnimCommand::doIt( const MArgList& )
{
	MStatus status( MStatus::kSuccess );
	if( !anim.bCommandQueued )
	{
		status = MStatus::kFailure;
		status.perror( "MayaFPS: The command can only be invoked by MayaFPS!" );
		return status;
	}
	anim.bCommandQueued = FALSE;

	if( anim.snapshots.empty() )
	{
		status = MStatus::kFailure;
		status.perror( "MayaFPS: No samples!!" );
		return status;
	}
	//cout << "MayaFPS anim command doing it." << endl;

	m_objCameraTransformNode = anim.objCameraTransformNode;
	m_objTranslationAttr = anim.objTranslationAttr;
	m_objRotationAttr = anim.objRotationAttr;


	MPlug plugTranslation( m_objCameraTransformNode, m_objTranslationAttr );
	MPlug plugRotation( m_objCameraTransformNode, m_objRotationAttr );

	MFnAnimCurve animCurveTX, animCurveTY, animCurveTZ, animCurveRX, animCurveRY, animCurveRZ;


	DisconnectIncoming( plugTranslation.child(0), m_DGModifier );
	MObject objTX = animCurveTX.create( plugTranslation.child(0), &m_DGModifier, &status );
	if( !status ) { status.perror( "MayaFPS: tx already animated!" ); return status; }

	DisconnectIncoming( plugTranslation.child(1), m_DGModifier );
	MObject objTY = animCurveTY.create( plugTranslation.child(1), &m_DGModifier, &status );
	if( !status ) { status.perror( "MayaFPS: ty already animated!" ); return status; }

	DisconnectIncoming( plugTranslation.child(2), m_DGModifier );
	MObject objTZ = animCurveTZ.create( plugTranslation.child(2), &m_DGModifier, &status );
	if( !status ) { status.perror( "MayaFPS: tz already animated!" ); return status; }

	DisconnectIncoming( plugRotation.child(0), m_DGModifier );
	MObject objRX = animCurveRX.create( plugRotation.child(0), &m_DGModifier, &status );
	if( !status ) { status.perror( "MayaFPS: rx already animated!" ); return status; }

	DisconnectIncoming( plugRotation.child(1), m_DGModifier );
	MObject objRY = animCurveRY.create( plugRotation.child(1), &m_DGModifier, &status );
	if( !status ) { status.perror( "MayaFPS: ry already animated!" ); return status; }

	DisconnectIncoming( plugRotation.child(2), m_DGModifier );
	MObject objRZ = animCurveRZ.create( plugRotation.child(2), &m_DGModifier, &status );
	if( !status ) { status.perror( "MayaFPS: rz already animated!" ); return status; }


	status = m_DGModifier.doIt();
	if( !status )
	{
		status.perror( "m_DGModifier.doIt() has failed to create animation curves." );
		return status;
	}

	//cout << "MayaFPS anim command bailing early" << endl;
	//return MStatus::kSuccess;


	for( unsigned i=0; i<anim.snapshots.size(); i++ )
	{
		const Anim::Snapshot& snapshot = anim.snapshots[i];

		double tm = snapshot.time - anim.snapshots[0].time;
		MTime mtm( tm, MTime::kMilliseconds );
		mtm += anim.startTime;

		animCurveTX.addKey( mtm, snapshot.translation.x );
		animCurveTY.addKey( mtm, snapshot.translation.y );
		animCurveTZ.addKey( mtm, snapshot.translation.z );

		animCurveRX.addKey( mtm, snapshot.rotation.x );
		animCurveRY.addKey( mtm, snapshot.rotation.y );
		animCurveRZ.addKey( mtm, snapshot.rotation.z );
	}

	// Do a euler filter to sort out rotation curves mangling.
	MString sMel = "filterCurve";
	sMel += " " + animCurveRX.name() + " " + animCurveRY.name() + " " + animCurveRZ.name();
	//status = m_DGModifier.commandToExecute( sMel );
	MGlobal::executeCommand( sMel );

	//cout << "MayaFPS anim command has successfully completed." << endl;
	return MStatus::kSuccess;
}




MStatus CAnimCommand::redoIt()
{
	return m_DGModifier.doIt();
}


MStatus CAnimCommand::undoIt()
{
	return m_DGModifier.undoIt();
}


void* Anim::CreateAnimCommand() { return new CAnimCommand; }
