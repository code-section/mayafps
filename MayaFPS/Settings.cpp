
# include "Settings.h"
# include <CSTB/tinyxml2/XML.h>
# include <CSTB/win32/Win32Util.h>

# include <Shlobj.h>
# include <shlwapi.h>
# pragma comment( lib, "Shell32.lib" )
# pragma comment( lib, "shlwapi.lib" )



TSettings settings;



TSettings::TSettings()
{
	general.displayWelcome = true;
	general.autoUpdate = true;
	general.consideredReleases = 0;
	general.checkOffers = true;
	general.consideredOffers = 0;
	general.displayRecordingMsg = true;
	general.totalNavUpdates = 0;
	general.licenseFileName[0] = 0;

	ResetUser();
}




void TSettings::ResetUser()
{
	engage.button = RButton;
	engage.ctrl = engage.alt = engage.shift = engage.win = false;

	controls.forward = 'W';
	controls.back = 'S';
	controls.left = 'A';
	controls.right = 'D';
	controls.up = 'E';
	controls.down = 'Q';
	controls.record = 'R';
	controls.settings = VK_F2;
	controls.welcome = VK_F1;

	movement.speed = 1.0;
	movement.runFactor = 2.5;
	movement.tweakFactor = 1.25;
	movement.horizontalFactor = 1.0;
	movement.verticalFactor = 1.0;
	movement.useCapslock = true;

	look.sensitivity = 1.0;
	look.tweakFactor = 1.025;
	look.smoothing = 50;
	look.smoothInterval = 150;

	tumblePivot.modifyTumblePivot = false;
	tumblePivot.modifyCOI = false;
	tumblePivot.pivotDistance = 15.0;
}




LPCTSTR TSettings::GetDefaultPath()
{
	static TCHAR sPath[ MAX_PATH ];
	sPath[0] = 0;

	SHGetSpecialFolderPath( NULL, sPath, CSIDL_APPDATA, FALSE );
	PathAppend( sPath, TEXT("MayaFPS.xml") );

	return sPath;
}




bool TSettings::Serialize( LPCTSTR sFile, bool bLoad )
{
	XML::Document file;
	if( bLoad && !file.Load( sFile ) )
		return false;

	auto hSettings = file.FindOrCreateElement( "mayafps_settings" );

	{
		auto hGeneral = hSettings.FindOrCreateChildElement( "general" );
		hGeneral.GetSetAttribute( "AutoUpdate", bLoad, general.autoUpdate );
		hGeneral.GetSetAttribute( "ShowWelcomeDlg", bLoad, general.displayWelcome );
		hGeneral.GetSetAttribute( "ConsideredReleases", bLoad, general.consideredReleases );
		hGeneral.GetSetAttribute( "CheckOffers", bLoad, general.checkOffers );
		hGeneral.GetSetAttribute( "ConsideredOffers", bLoad, general.consideredOffers );
		hGeneral.GetSetAttribute( "DisplayRecordingMsg", bLoad, general.displayRecordingMsg );
		hGeneral.GetSetAttribute( "dev_is_poor", bLoad, general.totalNavUpdates );
		hGeneral.GetSetAttribute( "licensePath", bLoad, general.licenseFileName, MAX_PATH );
	}

	{
		auto hEngage = hSettings.FindOrCreateChildElement( "engage" );
		hEngage.GetSetAttribute( "button", bLoad, engage.button );
		hEngage.GetSetAttribute( "ctrl", bLoad, engage.ctrl );
		hEngage.GetSetAttribute( "alt", bLoad, engage.alt );
		hEngage.GetSetAttribute( "shift", bLoad, engage.shift );
		hEngage.GetSetAttribute( "win", bLoad, engage.win );
	}

	{
		auto hControls = hSettings.FindOrCreateChildElement( "controls" );
		hControls.GetSetAttribute( "forward", bLoad, controls.forward );
		hControls.GetSetAttribute( "back", bLoad, controls.back );
		hControls.GetSetAttribute( "left", bLoad, controls.left );
		hControls.GetSetAttribute( "right", bLoad, controls.right );
		hControls.GetSetAttribute( "up", bLoad, controls.up );
		hControls.GetSetAttribute( "down", bLoad, controls.down );
		hControls.GetSetAttribute( "record", bLoad, controls.record );
		hControls.GetSetAttribute( "settings", bLoad, controls.settings );
		hControls.GetSetAttribute( "welcome", bLoad, controls.welcome );
	}

	{
		auto hMovement = hSettings.FindOrCreateChildElement( "movement" );
		hMovement.GetSetAttribute( "speed", bLoad, movement.speed );
		hMovement.GetSetAttribute( "runFactor", bLoad, movement.runFactor );
		hMovement.GetSetAttribute( "tweakFactor", bLoad, movement.tweakFactor );
		hMovement.GetSetAttribute( "horizontalFactor", bLoad, movement.horizontalFactor );
		hMovement.GetSetAttribute( "verticalFactor", bLoad, movement.verticalFactor );
		hMovement.GetSetAttribute( "useCapslock", bLoad, movement.useCapslock );
	}

	{
		auto hLook = hSettings.FindOrCreateChildElement( "look" );
		hLook.GetSetAttribute( "sensitivity", bLoad, look.sensitivity );
		hLook.GetSetAttribute( "tweakFactor", bLoad, look.tweakFactor );
		hLook.GetSetAttribute( "smoothing", bLoad, look.smoothing );
		hLook.GetSetAttribute( "smoothInterval", bLoad, look.smoothInterval );
	}

	{
		auto hTumblePivot = hSettings.FindOrCreateChildElement( "tumpePivot" );
		hTumblePivot.GetSetAttribute( "modifyTumblePivot", bLoad, tumblePivot.modifyTumblePivot );
		hTumblePivot.GetSetAttribute( "modfiyCOI", bLoad, tumblePivot.modifyCOI );
		hTumblePivot.GetSetAttribute( "pivotDistance", bLoad, tumblePivot.pivotDistance );
	}

	if( !bLoad )
		return file.Save( sFile );

	return true;
}