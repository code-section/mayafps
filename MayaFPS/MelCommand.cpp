// Contains the implementation for the 'mapafps' mel command.

# include <Maya/MPxCommand.h>
# include <Maya/MArgList.h>
# include "MayaFPS.h"
# include "Settings.h"
# include "ResString.h"

const char* g_sCommand = "mayafps";


enum Action
{
	NOTHING,
	TOGGLE,
	ENABLE,
	DISABLE,
	SETTINGS,
	HELP
};





class CMayaFPSCommand : public MPxCommand
{
public:
	CMayaFPSCommand() { }
	virtual ~CMayaFPSCommand() { }

	bool isUndoable() const { return false; }


	MStatus doIt( const MArgList& args )
	{
		Action action = NOTHING;

		MStatus status = MStatus::kSuccess;
		MString arg = args.asString( 0, &status );

		if( !status ) action = TOGGLE;
		else if( arg == "enable" || arg == "-enable" ) action = ENABLE;
		else if( arg == "disable" || arg == "-disable" ) action = DISABLE;
		else if( arg == "settings" || arg == "-settings" ) action = SETTINGS;
		else if( arg == "help" || arg == "-help" || arg == "?" ) action = HELP;
		else action = NOTHING;

		switch( action )
		{
		case NOTHING: OutputMessage( GET_STRING( IDS_ERR_INVALID_ARGUMENT ), MT_ERROR ); break;
		case SETTINGS: ShowSettingsDialog(); break;
		case HELP: ShowWelcomeDialog(); break;
		case TOGGLE: return Enable( !IsEnabled() ) ? MStatus::kSuccess : MStatus::kFailure;
		case ENABLE: return Enable( TRUE ) ? MStatus::kSuccess : MStatus::kFailure;
		case DISABLE: return Enable( FALSE ) ? MStatus::kSuccess : MStatus::kFailure;
		default:
			return MStatus::kFailure;
		}
		return MStatus::kSuccess;
	}
};



void* CreateMayaFPSCommand() { return new CMayaFPSCommand; }
