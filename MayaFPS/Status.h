# ifndef STATUS_H
# define STATUS_H




/// Simple status class - mostly used as a return type for functions and methods.
class Status
{
public:
	enum STATUS
	{
		FAILURE = 0,
		SUCCESS = 1,
		WARNING,
		INFO
	};

	STATUS		status;
	int			code;
	union
	{
		void* extraInfoPtr;
		int extraInfo;
	};
	int			stringID;
	int			line;
	
# ifdef DEBUG
	const char* function;

	Status& Function( const char* f ) { function = f; return *this; }
# endif


	bool operator !() const { return !((bool)*this); }
	operator bool() const { return status == SUCCESS; }
	operator int() const { return (int)status; }
	//bool operator == (bool b) const { return b == (status == SUCCESS); }
	operator STATUS() const { return status; }

	Status& Code( int c ) { code = c; return *this; }
	Status& StringID( int id ) { stringID = id; return *this; }
	Status& ExtraInfo( void* p ) { extraInfoPtr = p; return *this; }
	Status& ExtraInfo( int n ) { extraInfoPtr = 0; extraInfo = n; return *this; }
	Status& Line( int l ) { line = l; return *this; }

	Status() { Reset(); }
	Status( bool b ) { Reset(); status = b ? SUCCESS : FAILURE; }

	void Reset()
	{
		status = SUCCESS; code = 0; stringID = -1; extraInfoPtr = 0; line = 0;
# ifdef DEBUG
		function = 0;
# endif
	}
};

// The following allows us to write:
// return MAKE_STATUS( true/false ).Code( errorCode ).ExtraInfo( extraInfo );
// In debug builds, the returned status will contain the function name and line number.

inline Status MakeStatus( bool s ) { return Status( s ); }
inline Status MakeStatus( Status::STATUS s ) { Status st; st.status = s; return st; }

# ifdef DEBUG
# define MAKE_STATUS( s ) MakeStatus(s).Function( __FUNCTION__ ).Line( __LINE__ )
# else
# define MAKE_STATUS( s ) MakeStatus(s).Line( __LINE__ )
# endif

# endif // include guard