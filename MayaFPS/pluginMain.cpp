/** @file
This is the main plug-in file. It contains the entry point for the plug-in, initialization and uninitialization code.
*/


# include "MayaFPS.h"
# include "Settings.h"
# include "Animation.h"
# include <maya/MFnPlugin.h>
# include <Maya/MSceneMessage.h>
# include <Maya/MAnimControl.h>
# include <Maya/MDagPath.h>
# include <Maya/MGlobal.h>
# include "ResString.h"
# include <CSTB\win32\Win32Util.h>
# include "Globals.h"

# pragma comment( lib, "Foundation.lib" )
# pragma comment( lib, "OpenMaya.lib" )
# pragma comment( lib, "OpenMayaUI.lib" )
# pragma comment( lib, "OpenMayaAnim.lib" )



PluginGlobals globals;

LPCTSTR ProductName = TEXT("MayaFPS");
LPCTSTR ProductVersion = TEXT("1.4");


void* CreateMayaFPSCommand();
extern const char* g_sCommand;




namespace Globals
{
	MCallbackId		OnMayaExitCBID = 0;
	MStringArray	menuCommand_Settings;
	MStringArray	menuCommand_Enable;
};



BOOL IsEnabled() { return globals.bEnabled; }
BOOL IsEngaged() { return globals.bEngaged; }






BOOL Enable( BOOL bEnable )
{
	if( bEnable == IsEnabled() )
		return TRUE;

	Engage( FALSE );

	globals.bEnabled = bEnable;

	if( !Mouse_OnEnable( bEnable ) && bEnable == TRUE )
	{
		Enable( FALSE );
		return FALSE;
	}

	if( !KB_OnEnable( bEnable ) && bEnable == TRUE )
	{
		Enable( FALSE );
		return FALSE;
	}
	
	return TRUE;
}






BOOL Engage( BOOL bEngage )
{
	if( bEngage == IsEngaged() )
		return TRUE;

	if( !IsEnabled() )
	{
		OutputStatus( MAKE_STATUS( Status::INFO ).StringID( IDS_ERR_SANITY ) );
		return FALSE;
	}


	globals.bEngaged = bEngage;

	if( !Mouse_OnEngage( bEngage ) && bEngage == TRUE )
	{
		Engage( FALSE );
		return FALSE;
	}

	if( !KB_OnEngage( bEngage ) && bEngage == TRUE )
	{
		Engage( FALSE );
		return FALSE;
	}

	if( !Anim_OnEngage( bEngage ) && bEngage == TRUE )
	{
		Engage( FALSE );
		return FALSE;
	}
	return TRUE;
}






VOID UpdateView()
{
	MStatus status = MStatus::kSuccess;
	M3dView& view = globals.view;

	DWORD time = GetTickCount();
	DWORD deltaTime = time - globals.prevUpdateTime;
	globals.prevUpdateTime = time;

	MDagPath cameraDP;
	status = view.getCamera( cameraDP );
	if( !status )
	{
		OutputStatus( MAKE_STATUS( false ).StringID( IDS_ERR_VIEW_GET_CAMERA ) );
		return;
	}
	MFnCamera camera( cameraDP );

	MVector viewDir = camera.viewDirection( MSpace::kWorld );
	MVector upDir = camera.upDirection( MSpace::kWorld );
	MVector rightDir = viewDir ^ upDir;
	MPoint eyePoint = camera.eyePoint( MSpace::kWorld );

	BOOL bCameraModified = FALSE;

	Mouse_OnUpdate( view, camera, deltaTime, viewDir, upDir, rightDir,
		eyePoint, bCameraModified );

	KB_OnUpdate( view, camera, deltaTime, viewDir, upDir, rightDir,
		eyePoint, bCameraModified );

	if( bCameraModified )
	{
		settings.general.totalNavUpdates++;
		status = camera.set( eyePoint, viewDir, upDir,
			camera.horizontalFieldOfView(), camera.aspectRatio() );
		if( !status )
			MGlobal::displayError( GET_STRING( IDS_ERR_CAMERA_SET ).CStr() );

		if( settings.tumblePivot.modifyTumblePivot || settings.tumblePivot.modifyCOI )
		{
			MPoint tumblePivot = camera.tumblePivot( &status );
			MPoint coi = tumblePivot;

			MPoint newTumblePivot = eyePoint + viewDir * settings.tumblePivot.pivotDistance;

			if( settings.tumblePivot.modifyTumblePivot )
				camera.setTumblePivot( newTumblePivot );

			if( settings.tumblePivot.modifyCOI )
				camera.setCenterOfInterest( settings.tumblePivot.pivotDistance );
		}

		Anim_OnUpdate( view, camera, time, deltaTime );

		if( !MAnimControl::isPlaying() )
			view.refresh();
	}
}










VOID Cleanup(VOID* lpData )
{
	settings.Save( TSettings::GetDefaultPath() );

	Enable( false );
	if( Globals::OnMayaExitCBID )
	{
		MSceneMessage::removeCallback( Globals::OnMayaExitCBID );
		Globals::OnMayaExitCBID = 0;
	}
}





MStatus initializePlugin( MObject obj )
{
	//InitCommonControls();
	MStatus		status = MStatus::kSuccess;
	MFnPlugin	plugin( obj, "code-section", "1.0", "Any" );
	
	// Initialize module dll info.
	globals.loadPath = plugin.loadPath();
	globals.filePath = globals.loadPath + MString( TEXT("/") ) + plugin.name() + MString(TEXT(".mll"));
	globals.hInstance = GetModuleHandle( globals.filePath.asWChar() );

	if( !globals.hInstance )
	{
		OutputStatus( MAKE_STATUS( false ).StringID( IDS_ERR_HINSTANCE ) );
		OutputMessage( globals.filePath.asWChar() );
		return MStatus::kFailure;
	}

	Status s;

	Globals::OnMayaExitCBID = MSceneMessage::addCallback( MSceneMessage::kMayaExiting, Cleanup, NULL );
	

	if( !settings.Load( TSettings::GetDefaultPath() ) )
	{
		MString msg("Failed to load settings file. Is this the first run? File: ");
		msg += TSettings::GetDefaultPath();
		OutputMessage( msg.asWChar(), MT_WARNING );
	}

	status = plugin.registerCommand( anim.sCommand, Anim::CreateAnimCommand );
	if( !status )
		return status;

	status = plugin.registerCommand( g_sCommand, CreateMayaFPSCommand );
	if( !status )
	{
		OutputStatus( MAKE_STATUS(false), TEXT("registerCommand has failed") );
		return status;
	}

	Enable( TRUE );

	Globals::menuCommand_Settings = plugin.addMenuItem( "MayaFPS Settings", "View", "mayafps", "settings" );
	Globals::menuCommand_Enable = plugin.addMenuItem( "Enable/Disable MayaFPS", "View", "mayafps", "" );

	if( settings.general.displayWelcome ) ShowWelcomeDialog();
	if( settings.general.autoUpdate ) CheckUpdates( FALSE );
	
	return MStatus::kSuccess;
}




MStatus uninitializePlugin( MObject obj )
{
	MStatus		status = MStatus::kSuccess;
	MFnPlugin	plugin( obj );

	Enable( FALSE );

	plugin.removeMenuItem( Globals::menuCommand_Settings );
	plugin.removeMenuItem( Globals::menuCommand_Enable );

	status = plugin.deregisterCommand( g_sCommand );

	status = plugin.deregisterCommand( anim.sCommand );

	Cleanup(NULL);

	return status;
}
