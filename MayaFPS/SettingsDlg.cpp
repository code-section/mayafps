# include "MayaFPS.h"
# include "Settings.h"
# include "resource.h"
# include <CSTB/win32/Dialog.h>
# include <CSTB/win32/Win32Util.h>
# include <windowsx.h>
# include <stdio.h>
# include <tchar.h>
# include "ResString.h"
# include "Globals.h"





WORD HotKey_Get( HWND hHotKey ) { return (WORD)SendMessage( hHotKey, HKM_GETHOTKEY, 0, 0 ); }
VOID HotKey_Set( HWND hHotKey, WORD key ) { SendMessage( hHotKey, HKM_SETHOTKEY, key, 0 ); }
VOID HotKey_PreventCombs( HWND hHotKey ) { SendMessage( hHotKey, HKM_SETRULES,
													   HKCOMB_S | HKCOMB_C | HKCOMB_A |
													   HKCOMB_SC | HKCOMB_SA | HKCOMB_CA | HKCOMB_SCA,
													   HKCOMB_NONE ); }




BOOL IntFromEdit( HWND hEdit, INT& num )
{
	if( !hEdit ) return FALSE;
	TCHAR s[8] = {0};
	INT len = GetWindowText( hEdit, s, ARRAY_SIZE(s) );
	if( len == 0 )
		return FALSE;
	return 1 == _sntscanf_s( s, len, TEXT("%d"), &num );
}




BOOL IntToEdit( HWND hEdit, INT num )
{
	if( !hEdit )
		return FALSE;
	TCHAR s[8] = {0};
	INT len = FormatString( s, ARRAY_SIZE(s), TEXT("{0}"), num );
	s[ ARRAY_SIZE(s)-1 ] = 0;
	//SetWindowText( hEdit, s );
	SendMessage( hEdit, WM_SETTEXT, 0, (LPARAM)s );
	return TRUE;
}




BOOL DoubleFromEdit( HWND hEdit, double& num )
{
	if( !hEdit ) return FALSE;
	TCHAR s[8] = {0};
	INT len = GetWindowText( hEdit, s, ARRAY_SIZE(s) );
	if( len == 0 )
		return FALSE;
	float f = 0.f;
	if( 1 != _sntscanf_s( s, len, TEXT("%f"), &f ) )
		return false;
	num = (double)f;
	return true;
}




BOOL DoubleToEdit( HWND hEdit, double num )
{
	if( !hEdit )
		return FALSE;
	TCHAR s[8] = {0};
	INT len = FormatString( s, ARRAY_SIZE(s), TEXT("{0:f4}"), num );
	s[ ARRAY_SIZE(s)-1 ] = 0;
	//SetWindowText( hEdit, s );
	SendMessage( hEdit, WM_SETTEXT, 0, (LPARAM)s );
	return TRUE;
}





class SettingsDialog : public CSTB::Dialog
{
public:

	void UpdateSettings()
	{
		//TSettings& settings = ::settings;

		settings.general.checkOffers = Button_GetCheck( GetDlgItem( hWnd, IDC_CHECK_OFFERS ) ) == BST_CHECKED;

		settings.engage.ctrl = Button_GetCheck( GetDlgItem( hWnd, IDC_CTRL ) ) == BST_CHECKED;
		settings.engage.alt = Button_GetCheck( GetDlgItem( hWnd, IDC_ALT ) ) == BST_CHECKED;
		settings.engage.shift = Button_GetCheck( GetDlgItem( hWnd, IDC_SHIFT ) ) == BST_CHECKED;
		settings.engage.win = Button_GetCheck( GetDlgItem( hWnd, IDC_WIN ) ) == BST_CHECKED;
		settings.engage.button = (TSettings::EMouseButton)ComboBox_GetCurSel( GetDlgItem( hWnd, IDC_ACTIVATE_ON ) );

		settings.controls.forward = HotKey_Get( GetDlgItem( hWnd, IDC_HK_FORWARD ) );
		settings.controls.back = HotKey_Get( GetDlgItem( hWnd, IDC_HK_BACK ) );
		settings.controls.left = HotKey_Get( GetDlgItem( hWnd, IDC_HK_LEFT ) );
		settings.controls.right = HotKey_Get( GetDlgItem( hWnd, IDC_HK_RIGHT ) );
		settings.controls.up = HotKey_Get( GetDlgItem( hWnd, IDC_HK_UP ) );
		settings.controls.down = HotKey_Get( GetDlgItem( hWnd, IDC_HK_DOWN ) );
		settings.controls.record = HotKey_Get( GetDlgItem( hWnd, IDC_HK_RECORD ) );
		settings.controls.settings = HotKey_Get( GetDlgItem( hWnd, IDC_HK_SETTINGS ) );
		settings.controls.welcome = HotKey_Get( GetDlgItem( hWnd, IDC_HK_WELCOME ) );

		// TODO: Add validation to the values assigned.
		double d = 0;
		int n = 0;
		if( DoubleFromEdit( GetDlgItem( hWnd, IDC_MOVEMENT_SPEED ), d ) )
			settings.movement.speed = d;
		if( DoubleFromEdit( GetDlgItem( hWnd, IDC_WALK_RUN_FACTOR ), d ) )
			settings.movement.runFactor = d;
		if( DoubleFromEdit( GetDlgItem( hWnd, IDC_MOVE_TWEAK_FACTOR ), d ) )
			settings.movement.tweakFactor = d;

		if( DoubleFromEdit( GetDlgItem( hWnd, IDC_HORIZONTAL_SPEED_FACTOR ), d ) )
			settings.movement.horizontalFactor = d;

		if( DoubleFromEdit( GetDlgItem( hWnd, IDC_VERTICAL_SPEED_FACTOR ), d ) )
			settings.movement.verticalFactor = d;

		settings.movement.useCapslock = Button_GetCheck( GetDlgItem( hWnd, IDC_CAPSLOCK ) ) == BST_CHECKED;

		if( DoubleFromEdit( GetDlgItem( hWnd, IDC_SENSITIVITY ), d ) )
			settings.look.sensitivity = d;
		if( DoubleFromEdit( GetDlgItem( hWnd, IDC_LOOK_TWEAK_FACTOR ), d ) )
			settings.look.tweakFactor = d;
		if( IntFromEdit( GetDlgItem( hWnd, IDC_SMOOTHING ), n ) )
			settings.look.smoothing = n;
		if( IntFromEdit( GetDlgItem( hWnd, IDC_INTERVAL ), n ) )
			settings.look.smoothInterval = n;

		// Quick validation for the settings values.
		settings.movement.speed = MAX( settings.movement.speed, 0.001 );
		settings.movement.tweakFactor = MAX( settings.movement.tweakFactor, 0.0001 );
		settings.movement.runFactor = MAX( settings.movement.runFactor, 1.1 );

		settings.look.sensitivity = CLAMP( settings.look.sensitivity, 0.0001, 20 );
		settings.look.smoothing = CLAMP( settings.look.smoothing, 0, 100 );
		settings.look.smoothInterval = CLAMP( settings.look.smoothInterval, 0, 3000 );
		settings.look.tweakFactor = MAX( settings.look.tweakFactor, 0.0001 );

		settings.tumblePivot.modifyTumblePivot = Button_GetCheck( GetDlgItem( hWnd, IDC_MODIFY_TUMBLE_PIVOT ) ) == BST_CHECKED;
		settings.tumblePivot.modifyCOI = Button_GetCheck( GetDlgItem( hWnd, IDC_MODIFY_COI ) ) == BST_CHECKED;
		DoubleFromEdit( GetDlgItem( hWnd, IDC_TUMBLE_PIVOT_DISTANCE ), settings.tumblePivot.pivotDistance );
	}



	VOID UpdateControls()
	{
		//TSettings& settings = *m_pSettings;

		Button_SetCheck( GetDlgItem( hWnd, IDC_CHECK_OFFERS ), settings.general.checkOffers );

		Button_SetCheck( GetDlgItem( hWnd, IDC_WIN ), settings.engage.win );
		Button_SetCheck( GetDlgItem( hWnd, IDC_CTRL ), settings.engage.ctrl );
		Button_SetCheck( GetDlgItem( hWnd, IDC_ALT ), settings.engage.alt );
		Button_SetCheck( GetDlgItem( hWnd, IDC_SHIFT ), settings.engage.shift );

		HotKey_Set( GetDlgItem( hWnd, IDC_HK_FORWARD ), settings.controls.forward );
		HotKey_Set( GetDlgItem( hWnd, IDC_HK_BACK ), settings.controls.back );
		HotKey_Set( GetDlgItem( hWnd, IDC_HK_LEFT ), settings.controls.left );
		HotKey_Set( GetDlgItem( hWnd, IDC_HK_RIGHT ), settings.controls.right );
		HotKey_Set( GetDlgItem( hWnd, IDC_HK_UP ), settings.controls.up );
		HotKey_Set( GetDlgItem( hWnd, IDC_HK_DOWN ), settings.controls.down );
		HotKey_Set( GetDlgItem( hWnd, IDC_HK_RECORD ), settings.controls.record );
		HotKey_Set( GetDlgItem( hWnd, IDC_HK_SETTINGS ), settings.controls.settings );
		HotKey_Set( GetDlgItem( hWnd, IDC_HK_WELCOME ), settings.controls.welcome );

		ComboBox_SetCurSel( GetDlgItem( hWnd, IDC_ACTIVATE_ON ), (INT)settings.engage.button );

		DoubleToEdit( GetDlgItem( hWnd, IDC_MOVEMENT_SPEED ), settings.movement.speed );
		DoubleToEdit( GetDlgItem( hWnd, IDC_MOVE_TWEAK_FACTOR ), settings.movement.tweakFactor );

		DoubleToEdit( GetDlgItem( hWnd, IDC_WALK_RUN_FACTOR ), settings.movement.runFactor );
		DoubleToEdit( GetDlgItem( hWnd, IDC_HORIZONTAL_SPEED_FACTOR ), settings.movement.horizontalFactor );
		DoubleToEdit( GetDlgItem( hWnd, IDC_VERTICAL_SPEED_FACTOR ), settings.movement.verticalFactor );
		Button_SetCheck( GetDlgItem( hWnd, IDC_CAPSLOCK ), settings.movement.useCapslock );

		DoubleToEdit( GetDlgItem( hWnd, IDC_SENSITIVITY ), settings.look.sensitivity );
		DoubleToEdit( GetDlgItem( hWnd, IDC_LOOK_TWEAK_FACTOR ), settings.look.tweakFactor );
		IntToEdit( GetDlgItem( hWnd, IDC_SMOOTHING ), settings.look.smoothing );
		IntToEdit( GetDlgItem( hWnd, IDC_INTERVAL ), settings.look.smoothInterval );

		Button_SetCheck( GetDlgItem( hWnd, IDC_MODIFY_TUMBLE_PIVOT ), settings.tumblePivot.modifyTumblePivot );
		Button_SetCheck( GetDlgItem( hWnd, IDC_MODIFY_COI ), settings.tumblePivot.modifyCOI );
		DoubleToEdit( GetDlgItem( hWnd, IDC_TUMBLE_PIVOT_DISTANCE ), settings.tumblePivot.pivotDistance );
	}


	BOOL OnInitDialog()
	{
		ComboBox_AddString( GetDlgItem( hWnd, IDC_ACTIVATE_ON ), GET_STRING(IDS_RMB).CStr() );
		ComboBox_AddString( GetDlgItem( hWnd, IDC_ACTIVATE_ON ), GET_STRING(IDS_XBUTTON1).CStr() );
		ComboBox_AddString( GetDlgItem( hWnd, IDC_ACTIVATE_ON ), GET_STRING(IDS_XBUTTON2).CStr() );

		// Prevent the hotkey controls from allowing the user to use a combination of keys for controls.
		// This is because we are not really using hotkeys but rather keyup/keydown messages while engaged,
		// so assigning single keys to functions makes more sense.
		HotKey_PreventCombs( GetDlgItem( hWnd, IDC_HK_FORWARD ) );
		HotKey_PreventCombs( GetDlgItem( hWnd, IDC_HK_BACK ) );
		HotKey_PreventCombs( GetDlgItem( hWnd, IDC_HK_LEFT ) );
		HotKey_PreventCombs( GetDlgItem( hWnd, IDC_HK_RIGHT ) );
		HotKey_PreventCombs( GetDlgItem( hWnd, IDC_HK_UP ) );
		HotKey_PreventCombs( GetDlgItem( hWnd, IDC_HK_DOWN ) );
		HotKey_PreventCombs( GetDlgItem( hWnd, IDC_HK_RECORD ) );
		HotKey_PreventCombs( GetDlgItem( hWnd, IDC_HK_SETTINGS ) );
		HotKey_PreventCombs( GetDlgItem( hWnd, IDC_HK_WELCOME ) );

		UpdateControls();
		return TRUE;
	}

	//BOOL OnClose( OnCommand( NULL, IDCANCEL, 0 ); }


	LRESULT OnCommand( HWND hCtrl, WORD id, WORD code )
	{
		//if( !m_pSettings ) return Dialog::OnCommand( hCtrl, id, code );
		//TSettings& settings = *m_pSettings;

		switch( id )
		{
		case IDC_RESET_ALL:
			settings.ResetUser();
			UpdateControls();
			return 0;

		case IDC_APPLY:
			UpdateSettings();
			UpdateControls();
			return 0;

		case IDOK:
			UpdateSettings();
			// fall through
		case IDCANCEL:
			DestroyWindow( hWnd );
			hWnd = NULL;
			return 0;
		}
		return Dialog::OnCommand( hCtrl, id, code );
	}
};




SettingsDialog settingsDialog;




BOOL ShowSettingsDialog()
{
	if( settingsDialog.hWnd )
	{
		SetActiveWindow( settingsDialog );
		return TRUE;
	}
	return settingsDialog.Create( globals.hInstance, M3dView::applicationShell(), MAKEINTRESOURCE( IDD_SETTINGS ) );
}




void UpdateSettingsDialog()
{
	if( settingsDialog.hWnd )
		settingsDialog.UpdateControls();
}
