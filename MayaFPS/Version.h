// MayaFPS constants.

# ifndef MAYAFPS_VERSION_H
# define MAYAFPS_VERSION_H

# include <windows.h>

# ifndef MAKEFOURCC
# define MAKEFOURCC(ch0, ch1, ch2, ch3)                              \
                ((DWORD)(BYTE)(ch0) | ((DWORD)(BYTE)(ch1) << 8) |   \
                ((DWORD)(BYTE)(ch2) << 16) | ((DWORD)(BYTE)(ch3) << 24 ))
# endif

# define PRODUCT_NAME			TEXT("MayaFPS")
# define PLUGIN_NAME			PRODUCT_NAME
# define PRODUCT_4CC			MAKEFOURCC('M','F','P','S')
# define LICENSE_VERSION		MAKELONG(2,1)


# endif // include guard