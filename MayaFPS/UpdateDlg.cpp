# include "Globals.h"
# include "ResString.h"
# include "MayaFPS.h"
# include "Settings.h"
# include "resource.h"
# include <CSTB/win32/Dialog.h>
# include <CSTB/win32/Win32Util.h>
# include <windowsx.h>
# include <shellapi.h>



class CUpdateDialog : public CSTB::Dialog
{
public:
	INT numReleases;

	BOOL Create( const CStringA& version, const CStringA& desc, INT _numReleases )
	{
		numReleases = _numReleases;

		if( hWnd ) // Already created
		{
			SetActiveWindow( hWnd );
			return FALSE;
		}

		if( !Dialog::Create( globals.hInstance, M3dView::applicationShell(), MAKEINTRESOURCE( IDD_UPDATE ) ) )
			return FALSE;

		/*
		const char* sDesc = desc.CStr();

		char sDescNoTabs[ 10240 ];
		sDescNoTabs[0] = 0;
		ReplaceString( sDescNoTabs, ARRAY_SIZE(sDescNoTabs), sDesc, "\t", " " );
		sDesc = sDescNoTabs;

		char sDescFinal[ 4096 ];
		sDescFinal[0] = 0;
		INT len = ReplaceString( sDescFinal, ARRAY_SIZE( sDescFinal ), sDesc, "\n", "\r\n" );

		//SaveBufferToFile( TEXT("C:\\Users\\Adel\\Desktop\\processed.txt"), sDescFinal, len+1 );

		CString sUpdateInfo;
		sUpdateInfo.Format( TEXT("{0} {1}. Update summary:\r\n{2}"), Globals::DLLProductName.asChar(), version, sDescFinal );
		SetWindowText( GetDlgItem( hWnd, IDC_UPDATE_SUMMARY ), sUpdateInfo );
		*/

		Button_SetCheck( GetDlgItem( hWnd, IDC_AUTO_UPDATE ),
			settings.general.autoUpdate ? BST_CHECKED : BST_UNCHECKED );

		Button_SetCheck( GetDlgItem( hWnd, IDC_SKIP_UPDATE ),
			settings.general.consideredReleases == numReleases ? BST_CHECKED : BST_UNCHECKED );
		return TRUE;
	}


	/*BOOL OnInitDialog()
	{	
		return TRUE;
	}*/


	LRESULT OnCommand( HWND hCtrl, WORD id, WORD code )
	{
		switch( id )
		{
		case IDC_AUTO_UPDATE:
			settings.general.autoUpdate = (BST_CHECKED == Button_GetCheck(hCtrl));
			return 0;

		case IDC_SKIP_UPDATE:
			settings.general.consideredReleases = ((BST_CHECKED == Button_GetCheck(hCtrl) ) ? numReleases : 0);
			return 0;

		case IDC_VISIT_WEBSITE:
			ShellExecute( hWnd, TEXT("open"), GET_STRING(IDS_UPDATE_URL), NULL, NULL, SW_SHOWNORMAL );
			return 0;

		case IDOK:
			// fall through
		case IDCANCEL:
			DestroyWindow( hWnd );
			hWnd = NULL;
			return 0;
		}
		return Dialog::OnCommand( hCtrl, id, code );
	}

} g_updateDialog;




BOOL ShowUpdateDialog( const CStringA& sVersion, const CStringA& sDesc, INT numReleases )
{ return g_updateDialog.Create( sVersion, sDesc, numReleases ); }