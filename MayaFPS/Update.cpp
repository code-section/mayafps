
# include "Globals.h"
# include "ResString.h"
# include "MayaFPS.h"
# include "Settings.h"
# include <tchar.h>
# include <stdio.h>
# include <CSTB/tinyxml2/XML.h>
# include <CSTB/win32/Win32Util.h>
# include <CSTB/win32/Thread.h>



Status GetLatestVersionInfo( CStringA& sVersion, CStringA& sDesc, INT& numReleases )
{
	CHAR response[ 1024 * 1024 ];
	response[0] = 0;

	DWORD bytesRead = 0;
	if( !LoadWebPage( GET_STRING_A( IDS_RELEASES_URL ), response, ARRAY_SIZE(response), &bytesRead ) )
		return MAKE_STATUS( false ).StringID( IDS_ERR_CONNECT ).Code( GetLastError() );

	//SaveBufferToFile( TEXT("C:\\Users\\Adel\\Desktop\\response.txt"), response.GetBuffer(), response.Size() );
	XML::Document releaseInfo;
	if( !releaseInfo.Parse( response ) )
		return MAKE_STATUS( false ).StringID( IDS_ERR_PARSE_UPDATE_DATA ).Code( releaseInfo.ErrorID() );

	auto hReleases = releaseInfo.GetFirst();
	auto hLatest = hReleases.FirstChildElement();
	numReleases = hReleases.CountChildren();
	if( !hLatest )
		return MAKE_STATUS( false ).StringID( IDS_ERR_PARSE_UPDATE_DATA ).Code( releaseInfo.ErrorID() );
	sVersion.Set( hLatest.GetAttribute( "version" ) );
	sDesc.Set( hLatest.GetText() );

	return true;
}





// A thread used to get information about the latest version of the plugin.
class CUpdateThread : public CSTB::Thread
{
public:
	CStringA	sVersion;
	CStringA	sDesc;
	INT			numReleases;
	Status		status;

	UINT ThreadMain( LPARAM lParam )
	{
		numReleases = 0;
		status = GetLatestVersionInfo( sVersion, sDesc, numReleases );
		return 0;
	}
} g_UpdateThread;






namespace UpdateParams
{
	HWND hButton = NULL;
	BOOL bExplicit = FALSE;
};





BOOL ShowUpdateDialog( const CStringA& sVersion, const CStringA& sDesc, INT numReleases );


VOID CALLBACK UpdateThreadCheck( HWND hWnd, UINT msg, UINT_PTR idEvent, DWORD tickCount )
{
	if( g_UpdateThread.GetThreadStatus() )
		return; // Thread still running, nothing to do.

	KillTimer( NULL, idEvent );
	if( IsWindow( UpdateParams::hButton ) )
		EnableWindow( UpdateParams::hButton, TRUE );

	if( !g_UpdateThread.status )
	{
		// Could not get latest version information from server.
		if( UpdateParams::bExplicit )
			OutputStatus( g_UpdateThread.status );
		return;
	}

	MString msLatestVersion( g_UpdateThread.sVersion.CStr() );
	if( msLatestVersion == ProductVersion )
	{
		// No updates.
		if( UpdateParams::bExplicit )
			OutputMessage( GET_STRING( IDS_NO_UPDATES ), MT_INFO );
	}
	else
	{
		// There is an update. If the user has previously chosen to ignore it and the update check
		// is not explicit, don't show the dialog.
		if( !UpdateParams::bExplicit && settings.general.consideredReleases == g_UpdateThread.numReleases )
			return;

		ShowUpdateDialog( g_UpdateThread.sVersion, g_UpdateThread.sDesc, g_UpdateThread.numReleases );
	}
}






BOOL CheckUpdates( BOOL bExplicit, HWND hUpdateButton )
{
	UpdateParams::bExplicit = bExplicit;
	UpdateParams::hButton = hUpdateButton;

	if( g_UpdateThread.GetThreadStatus() )
		return FALSE; // Update thread already running.

	g_UpdateThread.StartThread();
	SetTimer( NULL, 0, 200, UpdateThreadCheck );
	if( IsWindow( UpdateParams::hButton ) )
		EnableWindow( UpdateParams::hButton, FALSE );
	return TRUE;
}