# ifndef MAYAFPS_SETTINGS_H
# define MAYAFPS_SETTINGS_H


# include <windows.h>




struct TSettings
{
	TSettings();

	VOID ResetUser();
	bool Load( LPCTSTR sFile ) { return Serialize( sFile, true ); }
	bool Save( LPCTSTR sFile ) { return Serialize( sFile, false ); }

	static LPCTSTR GetDefaultPath();



	struct
	{
		bool autoUpdate;
		bool displayWelcome;
		int consideredReleases; // The number of releases considered. This affects the
								// "Don't notify me until the next update" option.
		bool checkOffers;		// Check for new offers and related software notifications.
		int consideredOffers;	// The number of offers considered.
		bool displayRecordingMsg; // Whether to display the "Recording Started/finished" messages.
		int totalNavUpdates;
		TCHAR licenseFileName[ MAX_PATH ];

	} general;


	// The settings dialog relies on the members of this enum being valid array indices for
	// use with the combo box used for selecting the engage button.
	typedef int EMouseButton;
	enum
	{
		RMB = 0,
		XMB1,
		XMB2,
		RButton = RMB,
		XButton1 = XMB1,
		XButton2 = XMB2,
	};


	struct
	{
		EMouseButton button;
		bool ctrl;
		bool alt;
		bool shift;
		bool win;
	} engage;


	struct
	{
		INT forward;
		INT back;
		INT left;
		INT right;
		INT up;
		INT down;
		INT record;
		INT settings;
		INT welcome;
	} controls;


	struct
	{
		double speed;
		double runFactor;
		double tweakFactor; // granularity??
		double horizontalFactor;
		double verticalFactor;
		bool useCapslock;
	} movement;


	struct
	{
		double sensitivity;
		double tweakFactor;
		int smoothing; // 0 to 100, 0 = no smoothing.
		int smoothInterval;
	} look;


	struct
	{
		bool	modifyTumblePivot;
		bool	modifyCOI;
		double	pivotDistance;
	} tumblePivot;



protected:
	bool Serialize( LPCTSTR sFile, bool bLoad );
};



extern TSettings settings;


BOOL ShowSettingsDialog();
void UpdateSettingsDialog();



# endif // inclusion guard