// Keyboard-related stuff for the Maya FPS plugin.


# include "MayaFPS.h"
# include <CSTB\win32\Win32Util.h>
# include <CSTB\win32\Window.h>
# include "Settings.h"
# include <Maya/MFnCamera.h>
# include <Maya/MGlobal.h>
# include "ResString.h"
# include "Globals.h"




namespace KB
{
	enum MoveCommand
	{
		FORWARD = 0,
		BACK,
		LEFT,
		RIGHT,
		UP,
		DOWN
	};

	struct TMoveKey
	{
		MoveCommand command;
		bool state;	
	};

	TMoveKey moveKeys[] =
	{
		FORWARD, false,
		BACK, false,
		LEFT, false,
		RIGHT, false,
		UP, false,
		DOWN, false
	};


	enum PluginCommand
	{
		RECORD,
		SETTINGS,
		WELCOME
	};
	
	
	const DWORD		updateInterval = USER_TIMER_MINIMUM;
	UINT_PTR		timerID = 0;
	HHOOK			hook = NULL;
	BOOL			bFirstKeyDown = TRUE;
};




BOOL IsAnyMovementKeyDown()
{
	for( UINT i=0; i<ARRAY_SIZE( KB::moveKeys ); i++ )
		if( KB::moveKeys[i].state )
			return TRUE;
	return FALSE;
}




BOOL IsMoveKeyDown( KB::MoveCommand command )
{
	for( INT i=0; i<ARRAY_SIZE( KB::moveKeys ); i++ )
	{
		if( KB::moveKeys[i].command == command )
			return KB::moveKeys[i].state;
	}
	return FALSE;
}





bool GetMoveCommand( DWORD vk, KB::MoveCommand* pCommand )
{
	if( vk == 0 )
		return false;
	if( vk == settings.controls.forward ) { *pCommand = KB::FORWARD; return true; }
	if( vk == settings.controls.back ) { *pCommand = KB::BACK; return true; }
	if( vk == settings.controls.left ) { *pCommand = KB::LEFT; return true; }
	if( vk == settings.controls.right ) { *pCommand = KB::RIGHT; return true; }
	if( vk == settings.controls.up ) { *pCommand = KB::UP; return true; }
	if( vk == settings.controls.down ) { *pCommand = KB::DOWN; return true; }
	return false;
}



bool GetMoveKeyIndex( DWORD vk, INT* pIndex )
{
	KB::MoveCommand moveCommand;
	if( !GetMoveCommand( vk, &moveCommand ) )
		return false;
	*pIndex = (INT)moveCommand; // enum as index... this whole scheme is not elegant!

	return true;
}



bool IsCommandKey( DWORD vk, KB::PluginCommand* pCommand )
{
	if( vk == 0 )
		return false;
	if( vk == settings.controls.record ) { *pCommand = KB::RECORD; return true; }
	if( vk == settings.controls.settings ) { *pCommand = KB::SETTINGS; return true; }
	if( vk == settings.controls.welcome ) { *pCommand = KB::WELCOME; return true; }
	return false;
}





///////////////////////////////////////////////////////////////
// Update timer callback. The timer is enabled as long as a movement key is pressed.
//
VOID CALLBACK OnUpdateTimer( HWND hWnd, UINT Msg, UINT_PTR idEvent, DWORD dwTime )
{
	// Update the view unless there are messages in the message queue.
	if( GetQueueStatus( QS_ALLEVENTS ) )
	{
		//cout << "Skipping timed update since there are input messages already" << endl;
		//return;
	}
	if( !IsAnyMovementKeyDown() )
	{
		OutputStatus( MAKE_STATUS( Status::INFO ).StringID( IDS_ERR_SANITY ) );
		EnableKBUpdateTimer( FALSE );
	}
	else
		UpdateView();
}





////////////////////////////////////////////////////////////////
// Enables or disables the update timer. The timer interval is constant KB::updateInterval
BOOL EnableKBUpdateTimer( BOOL bEnable )
{
	// NOTE: timerID = 0 can still be a valid timer. This may be the source of the occasional bugs.
	BOOL bEnabled = KB::timerID != 0;
	if( bEnabled == bEnable )
		return TRUE;

	if( bEnable )
	{
		KB::bFirstKeyDown = TRUE;
		KB::timerID = SetTimer( NULL, 0, KB::updateInterval, OnUpdateTimer );
		return KB::timerID != 0;
	}
	// Deactivate timer.
	KillTimer( NULL, KB::timerID );
	KB::timerID = 0;
	return TRUE;
}






VOID KB_OnUpdate( M3dView& view, MFnCamera& camera, DWORD timeSincePrevUpdate,
				  MVector& viewDir, MVector& upDir, MVector& rightDir,
				  MPoint& eyePoint, BOOL& bCameraModified )
{
	if( !IsAnyMovementKeyDown() )
		return;
	bCameraModified = TRUE;

	double speed = settings.movement.speed;
	// Scale movement speed depending on elapsed time because this function is not called in a constant rate.
	// this is because mouse events are usually more frequent than the keyboard timer.
	// Skip the first update since engaged.
	if( KB::bFirstKeyDown )
		KB::bFirstKeyDown = FALSE;
	else
		speed *= timeSincePrevUpdate / (double)KB::updateInterval;

	MVector forward = viewDir;


	if( ISKEYDOWN( VK_LSHIFT ) )
		speed *= settings.movement.runFactor;
	else if( ISKEYDOWN( VK_LCONTROL ) )
		speed /= settings.movement.runFactor;

	if( settings.movement.useCapslock && (GetKeyState( VK_CAPITAL ) & 0x1) )
	{
		// Lock movement to the horizontal plane.
		if( MGlobal::isZAxisUp() )
			forward.z = 0;
		else
			forward.y = 0;

		if( forward.length() < 0.01 )
			forward = viewDir;
		else
			forward.normalize();
	}


	if( IsMoveKeyDown( KB::FORWARD ) )
		eyePoint += speed * forward;
	if( IsMoveKeyDown( KB::BACK ) )
		eyePoint -= speed * forward;
	if( IsMoveKeyDown( KB::LEFT ) )
		eyePoint -= speed * rightDir * settings.movement.horizontalFactor;
	if( IsMoveKeyDown( KB::RIGHT ) )
		eyePoint += speed * rightDir * settings.movement.horizontalFactor;
	if( IsMoveKeyDown( KB::UP ) )
		eyePoint += speed * upDir * settings.movement.verticalFactor;
	if( IsMoveKeyDown( KB::DOWN ) )
		eyePoint -= speed * upDir * settings.movement.verticalFactor;
}






// An object used to subclass the focus window. We need this to capture WM_MOUSEWHEEL messages,
// as well as set/kill the update timer when ther user presses/releases movement keys.
class FocusWindow : public CSTB::Window
{
public:
	LRESULT MsgProc( UINT Msg, WPARAM wParam, LPARAM lParam )
	{
		switch( Msg )
		{
		case WM_MOUSEWHEEL:
			{
				OutputMessage( TEXT("wm_mousewheel handler" ), MT_INFO );
				FLOAT scroll = (SHORT)HIWORD(wParam)/(FLOAT)WHEEL_DELTA;
				if( scroll == 0 ) // This shouldn't happen, but just in case.
					return 0;
				double d = settings.movement.tweakFactor * abs(scroll);
				if( scroll > 0 )
					settings.movement.speed *= d;
				else
				{
					settings.movement.speed /= d;
					settings.movement.speed = MAX( settings.movement.speed, 0.0001 );
				}
				UpdateSettingsDialog();
			}
			return 0;
		}
		return DefWndProc( Msg, wParam, lParam );
	}
};// g_FocusWindow;






VOID OnMoveKeyMessage( INT keyIndex, WPARAM Msg )
{
	KB::moveKeys[ keyIndex ].state = (Msg == WM_KEYDOWN || Msg == WM_SYSKEYDOWN);
	BOOL bMovementKeyDown = IsAnyMovementKeyDown();
	EnableKBUpdateTimer( bMovementKeyDown );
	if( !bMovementKeyDown && !IsButtonDown() )
		Engage( FALSE );
}





VOID OnCommandKeyMessage( KB::PluginCommand command, WPARAM Msg )
{
	if( Msg != WM_KEYDOWN && Msg != WM_SYSKEYDOWN )
		return;

	switch( command )
	{
# ifdef DEMO
	case KB::RECORD:
		OutputMessage( GET_STRING( IDS_FEATURE_NA ), MT_WARNING );
		break;
# else
	case KB::RECORD: Anim_StartRecording(); break;
# endif
	case KB::WELCOME: ShowWelcomeDialog(); break;
	case KB::SETTINGS: ShowSettingsDialog(); break;
	}
	UpdateSettingsDialog();
}




// Low level hook procedure. The hook is active while FPS controls are engaged.
// This function consumes all keyboard input while engaged, and keeps track of which
// keys are up and which are down.
LRESULT CALLBACK LLKBHookProc( INT code, WPARAM wParam, LPARAM lParam )
{
	KBDLLHOOKSTRUCT* pInfo = (KBDLLHOOKSTRUCT*)lParam;

	if( code < 0 )
		return CallNextHookEx( KB::hook, code, wParam, lParam );

	if( !IsEngaged() )
	{
		OutputStatus( MAKE_STATUS( Status::INFO ).StringID( IDS_ERR_SANITY ) );
		return CallNextHookEx( KB::hook, code, wParam, lParam );
	}

	if( wParam == WM_KEYDOWN || wParam == WM_KEYUP ||
		(settings.engage.alt && (wParam == WM_SYSKEYDOWN || wParam == WM_SYSKEYUP) ) )
	{
		INT index = -1;
		KB::PluginCommand command;

		if( GetMoveKeyIndex( pInfo->vkCode, &index ) )
			OnMoveKeyMessage( index, wParam );
		else if( IsCommandKey( pInfo->vkCode, &command ) )
			OnCommandKeyMessage( command, wParam );
		else
		{
			//if( wParam == WM_KEYDOWN )
			//	return 1; // Eat all uninteresting keyboard input while engaged.
			return CallNextHookEx( KB::hook, code, wParam, lParam ); // Ignore uninteresting keys.
		}

		// Eat all movement and command keyboard messages.
		return 1;
	}
	return CallNextHookEx( KB::hook, code, wParam, lParam );
}





BOOL KB_OnEnable( BOOL bEnable ) { return TRUE; }



BOOL KB_OnEngage( BOOL bEngage )
{
	// Reset the movement key states to "not pressed".
	for( UINT i=0; i<ARRAY_SIZE(KB::moveKeys); i++ )
		KB::moveKeys[i].state = 0;

	if( bEngage )
	{
		// Set focus to the main Maya window, then subclass the focus window to gain access to mouse wheel messages.
		SetFocus( M3dView::applicationShell() );

		//HWND hFocus = GetFocus();
		//if( FALSE == g_FocusWindow.Subclass( hFocus ) )
			//OutputStatus( MAKE_STATUS( Status::FAILURE ).StringID( IDS_ERR_SUBCLASS ) );

		if( KB::hook != NULL )
			OutputStatus( MAKE_STATUS( Status::FAILURE ).StringID( IDS_ERR_SANITY ) );
		else
			KB::hook = SetWindowsHookEx( WH_KEYBOARD_LL, LLKBHookProc, globals.hInstance, 0 );

		if( !KB::hook )
			OutputStatus( MAKE_STATUS( Status::FAILURE ).StringID( IDS_ERR_HOOK_KB ) );
	}
	else
	{
		EnableKBUpdateTimer( FALSE );
		//g_FocusWindow.Detach();
		if( KB::hook )
		{
			UnhookWindowsHookEx( KB::hook );
			KB::hook = NULL;
		}
	}
	return TRUE;
}