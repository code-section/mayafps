
# include "Globals.h"
# include "ResString.h"



# define MAX_RES_STRING			4096


CStringA& LoadString( UINT id, CStringA& s )
{
	HMODULE hModule = globals.hInstance;
	s.Clear();
	if( !hModule )
		return s;

	CHAR sBuffer[ MAX_RES_STRING ];
	sBuffer[0] = 0;
	INT len = LoadStringA( hModule, id, sBuffer, MAX_RES_STRING );
	s.Set( sBuffer, len );
	return s;
}




CStringW& LoadString( UINT id, CStringW& s )
{
	HMODULE hModule = globals.hInstance;
	s.Clear();
	if( !hModule )
		return s;

	WCHAR sBuffer[ MAX_RES_STRING ];
	sBuffer[0] = 0;
	INT len = LoadStringW( hModule, id, sBuffer, MAX_RES_STRING );
	s.Set( sBuffer, len );
	return s;
}