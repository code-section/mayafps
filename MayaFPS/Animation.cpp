

# include "Animation.h"
# include "Globals.h"
# include "Settings.h"



Anim anim;






#pragma comment( lib, "winmm.lib" )
VOID Anim_StartRecording()
{
	if( anim.bRecording )
		return;

	MDagPath cameraDP;
	globals.view.getCamera( cameraDP );

	anim.objCameraTransformNode = cameraDP.transform();
	MFnDagNode transformDagNode( anim.objCameraTransformNode );
	anim.objTranslationAttr = transformDagNode.attribute( "translate" );
	anim.objRotationAttr = transformDagNode.attribute( "rotate" );
	// Also add "rotatePivot", "rotatePivotTranslate", "scalePivot", and "scalePivotTranslate" as needed.
	// Also, consider keying the "rotateQuaternion" attribute instead of "rotate".

	anim.snapshots.clear();
	anim.snapshots.reserve( 1200 );
	anim.startTime = MAnimControl::currentTime();
	anim.bRecording = TRUE;

	if( settings.general.displayRecordingMsg )
		MGlobal::displayInfo( "MayaFPS: Recording started" );
	//PlaySound(TEXT("SystemStart"), NULL, SND_ALIAS | SND_ASYNC);
}




VOID Anim_OnUpdate( M3dView& view, MFnCamera& camera, DWORD time, DWORD timeSincePrevUpdate )
{
	MStatus status;
	if( !anim.bRecording )
		return;

	MPlug t( anim.objCameraTransformNode, anim.objTranslationAttr );
	MPlug r( anim.objCameraTransformNode, anim.objRotationAttr );
	
	Anim::Snapshot snapshot;
	snapshot.translation = MVector(	t.child(0).asDouble(),
									t.child(1).asDouble(),
									t.child(2).asDouble() );
	snapshot.rotation = MVector(	r.child(0).asDouble(),
									r.child(1).asDouble(),
									r.child(2).asDouble() );
	snapshot.time = time;
	anim.snapshots.push_back( snapshot );
}




BOOL Anim_OnEngage( BOOL bEngage )
{
	MStatus status;
	if( bEngage )
		return TRUE; // We only want to add an animation curve when FPS controls are disengaged.

	if( !anim.bRecording || anim.snapshots.empty() )
		return TRUE;


	anim.bCommandQueued = TRUE;
	MGlobal::executeCommandOnIdle( anim.sCommand );


	// Stop recording.
	anim.bRecording = FALSE;
	if( settings.general.displayRecordingMsg )
		MGlobal::displayInfo( "MayaFPS: Recording finished" );
	//anim.snapshots.Clear( true );

	// Free some references
	/*anim.objCameraTransformNode = MObject::kNullObj;
	anim.objTranslationAttr = MObject::kNullObj;
	anim.objRotationAttr = MObject::kNullObj;*/

	return TRUE;
}