// Mouse-related stuff for the Maya FPS plugin.


# include "MayaFPS.h"
# include <windows.h>
# include <CSTB\win32\Window.h>
# include <CSTB\win32\Win32Util.h>
# include <Maya\MFnCamera.h>
# include <Maya\MMatrix.h>
# include <Maya\MDagPath.h>
# include <Maya\MGlobal.h>
# include "Settings.h"
# include "Globals.h"
# include "ResString.h"




namespace Mouse
{
	struct TSample
	{
		INT dx;
		INT dy;
		INT deltaTime;
	};

	TSample samples[] =
	{
		0, 0, 0,
		0, 0, 0,
		0, 0, 0,
		0, 0, 0,
		0, 0, 0,
		0, 0, 0,
		0, 0, 0,
		0, 0, 0,
		0, 0, 0,
		0, 0, 0,
	};
	INT		currentSample = 0;
	POINT	ptMouseStart;
	HHOOK	hHook = NULL;
	INT		RMBsToIgnore = 0;
	BOOL	bButtonDown = FALSE;
};



BOOL IsButtonDown() { return Mouse::bButtonDown; }



VOID OnBeginMouseTracking()
{
	GetCursorPos( &Mouse::ptMouseStart );
	Mouse::samples[0].deltaTime = 0;
	Mouse::samples[0].dx = 0;
	Mouse::samples[0].dy = 0;
	Mouse::currentSample = 1;
}




MMatrix& MatrixRotationAxis( MMatrix& matrix, const MVector& axis, double angle )
{
	MVector vAxis( axis.x, axis.y, axis.z );
	vAxis.normalize();

	double c = cos( angle );
	double s = sin( angle );
	double t = 1 - c;

	double x = vAxis.x;
	double y = vAxis.y;
	double z = vAxis.z;

	MMatrix& m = matrix; // Typing shortcut.
	m(0,0) = t*x*x + c;		m(0,1) = t*x*y - s*z;		m(0,2) = t*x*z + s*y;		m(0,3) = 0.f;
	m(1,0) = t*x*y + s*z;	m(1,1) = t*y*y + c;			m(1,2) = t*y*z - s*x;		m(1,3) = 0.f;
	m(2,0) = t*x*z - s*y;	m(2,1) = t*y*z + s*x;		m(2,2) = t*z*z + c;			m(2,3) = 0.f;
	m(3,0) = 0.f;			m(3,1) = 0.f;				m(3,2) = 0.f;				m(3,3) = 1.f;
	return m;
}


MVector& VectorRotateAxis( MVector& vector, const MVector& axis, double angle )
{
	MMatrix m;
	MatrixRotationAxis( m, axis, angle );
	vector = vector * m;
	vector.normalize();
	return vector;
}






// An object which subclasses the 3d viewport window. It handles mouse messages.
class ViewportWindow : public CSTB::Window
{
public:
	//HWND hPrevFocus;
	INT ignoreCount;
	ViewportWindow()
	{
		ignoreCount = 0;
		//hPrevFocus = NULL;
	}



	LRESULT MsgProc( UINT Msg, WPARAM wParam, LPARAM lParam )
	{
		// TODO: Call OnFocusMsgProc() if this is the focus window.
		switch( Msg )
		{
		case WM_MOUSEMOVE:
			if( IsEngaged() )
			{
				UpdateView();
				return 0;
			}
			break;



		case WM_LBUTTONDOWN: return 0;



		case WM_LBUTTONUP:
			if( IsEngaged() && settings.engage.button == TSettings::RButton )
			{
				// Should disengage and simulate RMB to engage maya's marking menu.
				Engage( FALSE );
				if( wParam & MK_RBUTTON )
				{
					//cout << "Simulating rbutton down" << endl;
					POINT pt;
					GetCursorPos(&pt);
					INPUT input = {0};
					input.type = INPUT_MOUSE;
					input.mi.dwFlags = MOUSEEVENTF_RIGHTDOWN;
					Mouse::RMBsToIgnore = 2; // TODO: Find out why, or just make sure it works.
					SendInput( 1, &input, sizeof(INPUT) );
					return 0;
				}
				else
					OutputStatus( MAKE_STATUS( false ).StringID( IDS_ERR_SANITY ) );
			}
			break;




		case WM_RBUTTONUP:
			if( settings.engage.button == TSettings::RButton )
			{
				Mouse::bButtonDown = FALSE;
				if( !IsAnyMovementKeyDown() )
					Engage( FALSE );
			}
			break;




		case WM_XBUTTONUP:
			if( (HIWORD(wParam) == XBUTTON1 && settings.engage.button == TSettings::XButton1 ) ||
				(HIWORD(wParam) == XBUTTON2 && settings.engage.button == TSettings::XButton2 ) )
			{
				Mouse::bButtonDown = FALSE;
				if( !IsAnyMovementKeyDown() )
					Engage( FALSE );
			}
			break;



		case WM_MOUSEWHEEL:
		{
			FLOAT scroll = (SHORT)HIWORD(wParam)/(FLOAT)WHEEL_DELTA;
			if( scroll == 0 ) // This shouldn't happen, but just in case.
				return 0;
			double d = settings.movement.tweakFactor * abs(scroll);
			if( scroll > 0 )
				settings.movement.speed *= d;
			else
			{
				settings.movement.speed /= d;
				settings.movement.speed = MAX( settings.movement.speed, 0.0001 );
			}
			UpdateSettingsDialog();
		}
		return 0;




		case WM_CAPTURECHANGED:
			if( IsEngaged() && (HWND)lParam != hWnd )
				Engage( FALSE );
			break;




		case WM_NCDESTROY:
			if( IsEngaged() )
				Engage( FALSE );
			Detach();
			break;
		}
		return DefWndProc( Msg, wParam, lParam );
	}
} g_ViewportWindow;






BOOL Mouse_OnEngage( BOOL bEngage )
{
	if( bEngage )
	{
		HWND hWnd = globals.view.window();
		if( !hWnd )
			return FALSE;

		// Subclass the 3D viewport window (the one which actually receives mouse input messages).
		if( FALSE == g_ViewportWindow.Subclass( hWnd ) )
		{
			OutputStatus( MAKE_STATUS( false ).StringID( IDS_ERR_SUBCLASS ) );
			return FALSE;
		}

		// Maya calls ShowCursor(TRUE) repeatedly for some reason while user is navigating.
		// Each call increments an OS counter, so a single call is not enough to hide the cursor.
		//cout << "Hiding cursor. Display count: " << ShowCursor( FALSE ) << endl;
		while( ShowCursor( FALSE ) >= 0 )
			;


		OnBeginMouseTracking();


		SetCapture( hWnd );
		// Now FPS mode is engaged.
	}
	else
	{
		ShowCursor( TRUE );
		if( GetCapture() == g_ViewportWindow )
			ReleaseCapture();
		g_ViewportWindow.Detach();
	}
	return TRUE;
}





// Handler for the WM_(R/X)BUTTONDOWN message.
// Returns TRUE if handled, FALSE otherwise (to let Maya handle it).
// Maya catches the WM_RBUTTONDOWN message and handles it inside its message processing loop,
// so the message never actually makes it to the window's message processor. This function is
// called from within the mouse hook procedure so we can catch the message before Maya does.
BOOL OnEngageButtonDown( HWND hWnd, UINT Msg )
{
	//cout << __FUNCTION__ << "messsage: " << ((Msg == WM_XBUTTONDOWN) ? "WM_XBUTTON" : "WM_RBUTTONDOWN") << endl;

	TSettings::EMouseButton engageButton = settings.engage.button;

	switch( engageButton )
	{
	case TSettings::RButton:
		if( Msg != WM_RBUTTONDOWN )
			return FALSE;
		if( Mouse::RMBsToIgnore > 0 )
		{
			//cout << __FUNCTION__ << ": Ignoring %d clicks" << Mouse::RMBsToIgnore << endl;
			Mouse::RMBsToIgnore--;
			return FALSE;
		}
		break;

	case TSettings::XButton1:
		if( Msg != WM_XBUTTONDOWN || !(GetAsyncKeyState((VK_XBUTTON1))&0x8000) )
			return FALSE;
		//cout << "XB1: RMBs to ignore: " << Mouse::RMBsToIgnore << endl;
		break;

	case TSettings::XButton2:
		if( Msg != WM_XBUTTONDOWN || !(GetAsyncKeyState((VK_XBUTTON2))&0x8000) )
			return FALSE;
		//cout << "XB2: RMBs to ignore: " << Mouse::RMBsToIgnore << endl;
		break;
	}

	Mouse::bButtonDown = TRUE;

	// Do some sanity checks.
	if( IsEngaged() )
	{
		//OutputStatus( MAKE_STATUS(Status::FAILURE, 0), GET_STRING(IDS_ERR_SANITY) );
		//Engage( FALSE );
		return TRUE;
	}

	// Let Maya handle it if alt, ctrl, or shift is down.
	bool alt = settings.engage.alt;
	bool ctrl = settings.engage.ctrl;
	bool shift = settings.engage.shift;
	bool win = settings.engage.win;
	bool altDown = ISKEYDOWN( VK_MENU );
	bool ctrlDown = ISKEYDOWN( VK_CONTROL );
	bool shiftDown = ISKEYDOWN( VK_SHIFT );
	bool winDown = ISKEYDOWN( VK_LWIN ) || ISKEYDOWN( VK_RWIN );


	if( alt != altDown ||
		ctrl != ctrlDown ||
		shift != shiftDown ||
		win != winDown )
	{
		return FALSE;
	}

	// Get active viewport.
	MStatus status;

	// This doesn't work since the user may right-click on a viewport other than the active one to
	// activate it and navigate it. So instead we get the viewport under the cursor.
	M3dView view;// = M3dView::active3dView( &status );
	BOOL bFoundView = FALSE;
	POINT ptCursor;
	GetCursorPos( &ptCursor );
	for( UINT i=0; i<M3dView::numberOf3dViews(); i++ )
	{
		M3dView v;
		status = M3dView::get3dView( i, v );
		if( !status )
			break;
		HWND hViewWnd = v.window();
		if( !IsWindow( hViewWnd ) )
			continue;
		RECT rViewWindow;
		GetWindowRect( hViewWnd, &rViewWindow );
		if( !PtInRect( &rViewWindow, ptCursor ) )
			continue;
		if( hWnd == hViewWnd || hWnd == GetParent( hViewWnd ) )
		//if( hClickWnd == hWnd || GetParent(hClickWnd) == hWnd )
		{
			//cout << "View found at index " << i << endl;
			view = v;
			bFoundView = TRUE;
			break;
		}
	}
	if( !status )
	{
		OutputStatus( MAKE_STATUS( false ).Code( status.statusCode() ).StringID( IDS_ERR_GET_VIEW ) );
		return FALSE;
	}

	if( !bFoundView )
		return FALSE; // Irrelevant RMB.


	MDagPath camDP;
	if( !view.getCamera( camDP ) )
		return FALSE;

	MFnCamera camera( camDP, &status );
	if( !status )
		return FALSE;

	if( camera.isOrtho() )
		return FALSE; // Don't engage on orthogonal views.

	globals.view = view;

	if( FALSE == Engage( TRUE ) )
		return 0;


	return 1; // Prevent system from sending WM_RBUTTONDOWN to Maya.
}






//////////////////////////////////////////////////////////
// This mouse hook procedure monitors mouse events in Maya.
//
LRESULT CALLBACK MouseHookProc( INT code, WPARAM wParam, LPARAM lParam )
{
	if( code < 0 )
		return CallNextHookEx( Mouse::hHook, code, wParam, lParam );


	MOUSEHOOKSTRUCT* pInfo = (MOUSEHOOKSTRUCT*)lParam;
	UINT Msg = (UINT)wParam;
	if( Msg == WM_RBUTTONDOWN || Msg == WM_XBUTTONDOWN )
		if( OnEngageButtonDown( pInfo->hwnd, Msg ) )
			return 1;

	return CallNextHookEx( Mouse::hHook, code, wParam, lParam );
}






////////////////////////////////////////////////////////////////
// Install or uninstall the hook procedure to monitor maya for mouse events.
// This is called by the Enable() function to install or uninstall the mouse hook procedure.
BOOL HookMouse( BOOL bHook )
{
	BOOL bHooked = Mouse::hHook != NULL;
	if( bHook == bHooked )
		return TRUE;

	if( bHook )
	{
		Mouse::hHook = SetWindowsHookEx( WH_MOUSE, MouseHookProc, GetModuleHandle(NULL), GetCurrentThreadId() );
		return Mouse::hHook != NULL;
	}

	UnhookWindowsHookEx( Mouse::hHook );
	Mouse::hHook = NULL;
	return TRUE;
}





VOID Mouse_OnUpdate( M3dView& view, MFnCamera& camera, DWORD timeSincePrevUpdate,
				  MVector& viewDir, MVector& upDir, MVector& rightDir,
				  MPoint& eyePoint, BOOL& bCameraModified )
{
	POINT ptCursor;
	GetCursorPos( &ptCursor );
	INT dx = ptCursor.x - Mouse::ptMouseStart.x;
	INT dy = ptCursor.y - Mouse::ptMouseStart.y;
	


	INT zero = 0;
	Mouse::RMBsToIgnore += zero;
	INT& iHook = *(INT*)&Mouse::hHook;
	iHook += zero;
	INT* pDX = &dx;
	pDX += zero;
	*pDX = dx + zero;

	if( dx == 0 && dy == 0 )
		return;

	INT iSample = Mouse::currentSample % ARRAY_SIZE( Mouse::samples );
	INT iPrevSample = (Mouse::currentSample-1) % ARRAY_SIZE( Mouse::samples );

	Mouse::TSample& sample = Mouse::samples[iSample];
	Mouse::TSample& prevSample = Mouse::samples[iPrevSample];

	sample.deltaTime = (INT)timeSincePrevUpdate;
	sample.dx = dx;
	sample.dy = dy;

	SetCursorPos( Mouse::ptMouseStart.x, Mouse::ptMouseStart.y );

	// Let's do some mouse smoothing. Determine the number of samples to use in the computation of
	// the new dx and dy values. We will smooth over a set amount of time (0.5 seconds for example).
	// The number of samples then is all samples in the list of samples we have which fall inside that
	// time frame.
	INT smoothTime = settings.look.smoothInterval;
	INT sampleCount = 0;
	INT sampleTime = 0;
	while( sampleCount < ARRAY_SIZE(Mouse::samples) )
	{
		INT iSample = (Mouse::currentSample - sampleCount) % ARRAY_SIZE( Mouse::samples );
		Mouse::TSample& sample = Mouse::samples[ iSample ];

		sampleCount++;

		sampleTime += sample.deltaTime;
		//cout << sampleTime << ". ";
		if( sampleTime > smoothTime )
			break;
	}

	if( sampleCount == 0 )
	{
		OutputStatus( MAKE_STATUS( false ).StringID( IDS_ERR_SANITY ) );
		return;
	}

	// Now we've determined the number of samples, do the smoothing.
	double sdx = 0, sdy = 0;
	for( INT i = 0; i < sampleCount; i++ )
	{
		INT iSample = (Mouse::currentSample - i) % ARRAY_SIZE( Mouse::samples );
		Mouse::TSample& sample = Mouse::samples[ iSample ];
		double s = 1 / (double)sampleCount;
		// Smooth step
		s = s * s * (3-2*s);
		sdx += sample.dx * s;
		sdy += sample.dy * s;
	}

	Mouse::currentSample++;

	// Lerp between full smoothing and no smoothing based on user preference.
	sdx = LERP( (double)dx, sdx, (double)settings.look.smoothing / 100 );
	sdy = LERP( (double)dy, sdy, (double)settings.look.smoothing / 100 );

	double rotSpeed = settings.look.sensitivity / 360;

	// Do rotation based on mouse movement.
	MVector yDir = MGlobal::upAxis();

	if( dx )
	{
		// Rotate the right direction around the y-axis.
		VectorRotateAxis( rightDir, yDir, sdx * rotSpeed );
		//VectorRotateAxis( rightDir, upDir, sdx * rotSpeed );

		// Recalculate up vector
		upDir = rightDir ^ viewDir;
	}

	if( dy )// Rotate the up direction around the right direction.
		VectorRotateAxis( upDir, rightDir, sdy * rotSpeed );

	// Recalculate the view direction.
	viewDir = upDir ^ rightDir;

	bCameraModified = TRUE;
}





BOOL Mouse_OnEnable( BOOL bEnable )
{
	if( bEnable )
	{
		if( !HookMouse( TRUE ) )
		{
			OutputStatus( MAKE_STATUS( false ).StringID( IDS_ERR_HOOK_MOUSE ) );
			return FALSE;
		}
	}
	else
		HookMouse( FALSE );
	return TRUE;
}
