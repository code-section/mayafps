/** @file
The main header file for the MayaFPS plug-in. This is where all global-space functions and structures are declared.
Global variables are in Globals.h.
*/

# ifndef MAYAFPS_H
# define MAYAFPS_H

# include <maya/MString.h>
# include <maya/M3dView.h>
# include <maya/MEvent.h>
# include <Maya/MfnCamera.h>
# include "Status.h"
# include "Version.h"
# include "ResString.h"


VOID		UpdateView();
BOOL		IsEnabled();
BOOL		Enable( BOOL bEnable );
BOOL		IsEngaged();
BOOL		Engage( BOOL bEngage );
BOOL		EnableKBUpdateTimer( BOOL bEnable );
BOOL		IsAnyMovementKeyDown();
BOOL		IsAnyCommandKeyDown();
BOOL		IsButtonDown(); ///< Returns whether the activation button is held down.

BOOL		ShowWelcomeDialog();
BOOL		ShowAboutDialog();
BOOL		CheckUpdates( BOOL bExplicit, HWND hUpdateButton = NULL );
BOOL		CheckOffers();

BOOL		Mouse_OnEngage( BOOL bEngage );
BOOL		Mouse_OnEnable( BOOL bEnable );
VOID		Mouse_OnUpdate( M3dView& view, MFnCamera& camera, DWORD timeSincePrevUpdate,
						  MVector& viewDir, MVector& upDir, MVector& rightDir,
						  MPoint& eyePoint, BOOL& bCameraModified );

BOOL		KB_OnEngage( BOOL bEngage );
BOOL		KB_OnEnable( BOOL bEnable );
VOID		KB_OnUpdate( M3dView& view, MFnCamera& camera, DWORD timeSincePrevUpdate,
						  MVector& viewDir, MVector& upDir, MVector& rightDir,
						  MPoint& eyePoint, BOOL& bCameraModified );

# ifndef DEMO
BOOL		Anim_OnEngage( BOOL bEngage );
VOID		Anim_StartRecording();
VOID		Anim_OnUpdate( M3dView& view, MFnCamera& camera, DWORD time, DWORD timeSincePrevUpdate );
# endif

enum MessageType
{
	MT_AUTO = 0,
	MT_COUT,
	MT_INFO,
	MT_WARNING,
	MT_ERROR
};


//VOID		StatusBox( HWND hParent, const Status& status, const TCHAR* msgExtra = NULL )
VOID		OutputStatus( const Status& s, const TCHAR* msgExtra = NULL );
VOID		OutputMessage( const TCHAR* sMsg, MessageType = MT_AUTO );


# endif // MAYAFPS_H