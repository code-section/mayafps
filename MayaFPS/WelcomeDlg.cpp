// Stuff related to the dialogs displayed by the plug-in.
// These are the welcome dialog (with the quick guide), and the settings dialog.


# include "MayaFPS.h"
# include "Settings.h"
# include <shellapi.h>
# include <stdio.h>
# include <tchar.h>
# include "Globals.h"
# include <CSTB/win32/Dialog.h>
# include <CSTB/win32/Win32Util.h>
# include <Windowsx.h>

# include "ResString.h"
# include "resource.h"




class CWelcomeDialog : public CSTB::Dialog
{
public:

	BOOL OnInitDialog()
	{
		// Set the always show checkbox state.
		Button_SetCheck( GetDlgItem( hWnd, IDC_ALWAYS_SHOW ), settings.general.displayWelcome ? BST_CHECKED : BST_UNCHECKED );

# if defined( DEMO )
		//EnableWindow( GetDlgItem( hWnd, IDC_SETTINGS ), FALSE );
		EnableWindow( GetDlgItem( hWnd, IDC_ALWAYS_SHOW ), FALSE );
# else
		ShowWindow( GetDlgItem( hWnd, IDC_VISIT_WEBSITE ), SW_HIDE );
		// TODO: Also hide the 'demo' message
# endif
		
		return TRUE;
	}


	LRESULT OnCommand( HWND hCtrl, WORD id, WORD code )
	{
		switch( id )
		{
		case IDC_ALWAYS_SHOW:
			settings.general.displayWelcome = (BST_CHECKED == Button_GetCheck(hCtrl));
			return 0;

		case IDC_SETTINGS:
			ShowSettingsDialog();
			return 0;

		case IDC_ABOUT:
			ShowAboutDialog();
			return 0;

		case IDC_VISIT_WEBSITE:
			ShellExecute( hWnd, TEXT("open"), GET_STRING(IDS_PRODUCT_URL), NULL, NULL, SW_SHOWNORMAL );
			return 0;

		case IDC_DOCS:
			ShellExecute( hWnd, TEXT("open"), GET_STRING(IDS_HELP_URL), NULL, NULL, SW_SHOWNORMAL );
			return 0;

		case IDOK:
			// fall through
		case IDCANCEL:
			DestroyWindow( hWnd );
			hWnd = NULL;
			return 0;
		}
		return Dialog::OnCommand( hCtrl, id, code );
	}

} g_welcomeDialog;




BOOL ShowWelcomeDialog()
{
	if( g_welcomeDialog.hWnd )
	{
		// Already shown. Activate it.
		SetActiveWindow( g_welcomeDialog.hWnd );
		return FALSE;
	}
	if( globals.hInstance == NULL )
	{
		cout << GET_STRING( IDS_ERR_INVALID_HMODULE ) << endl;
		return FALSE;
	}
	return g_welcomeDialog.Create( globals.hInstance, M3dView::applicationShell(), MAKEINTRESOURCE(IDD_WELCOME) );
}
