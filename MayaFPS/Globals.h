/** @file
This file contains the declaration for the general global variables used in MayaFPS.
There are other, less general, global variables declared in other headers.

@namespace Globals
The globals namespace contains all global variables that the plug-in uses.
*/

# ifndef MAYAFPS_GLOBALS_H
# define MAYAFPS_GLOBALS_H


# include <maya/MString.h>
# include <maya/M3DView.h>
# include <windows.h>


struct PluginGlobals
{
	DWORD prevUpdateTime = 0;
	bool bEnabled = false;
	bool bEngaged = false;
	M3dView view;
	HINSTANCE hInstance = 0;
	MString loadPath;
	MString filePath;
	DWORD engageTime = 0;
	int updateCount = 0;
};


extern LPCTSTR ProductName;
extern LPCTSTR ProductVersion;


extern PluginGlobals globals;

//namespace Globals
//{
//	GLOBALVAR_INIT( DWORD, prevUpdateTime, 0 );
//	GLOBALVAR_INIT( BOOL, bEnabled, FALSE );
//	GLOBALVAR_INIT( BOOL, bEngaged, FALSE ); ///< True while first person controls are engaged.
//	GLOBALVAR( M3dView, view ); ///< The active 3d view being navigated.
//	GLOBALVAR_INIT( HINSTANCE, hInstance, NULL ); ///< The win32 module handle of the plug-in.
//	GLOBALVAR( MString, loadPath );
//	GLOBALVAR( MString, filePath );
//	GLOBALVAR( MString, DLLProductName ); ///< The product name as read from the dll file's version resource.
//	GLOBALVAR( MString, DLLProductVersion ); ///< The product version as read from the dll file's version resource.
//# ifdef DEMO
//	GLOBALVAR_INIT( DWORD, engageTime, 0 );
//	GLOBALVAR_INIT( INT, updateCount, 0 );
//# endif
//};

# endif