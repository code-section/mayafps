
# include "MayaFPS.h"
# include "ResString.h"
# include <Maya/MGlobal.h>





VOID OutputStatus( const Status& status, const TCHAR* msgExtra )
{
	CString msg;
	if( status.stringID > 0 )
		msg = GET_STRING( status.stringID );
	const char* function = "";
	int line = status.line;
# ifdef DEBUG
	function = status.function;
# endif

	MessageType msgType = MT_AUTO;
	if( status.status == Status::FAILURE ) msgType = MT_ERROR;
	else if( status.status == Status::INFO ) msgType = MT_INFO;
	else if( status.status == Status::WARNING ) msgType = MT_WARNING;

	CString outMsg;
	outMsg.Format( TEXT("{0} - {1}:{2}:{3}:{4}:{5}"),
				msg,
				msgExtra ? msgExtra : TEXT(""),
				status.code,
				status.extraInfoPtr,
				line,
				function );
	OutputMessage( outMsg, msgType );
}





VOID OutputMessage( const TCHAR* s, MessageType type )
{
	CStringA str;
	str.Format( "{0}: {1}", PLUGIN_NAME, s );
	switch( type )
	{
	case MT_INFO:		MGlobal::displayInfo( str.CStr() );		return;
	case MT_WARNING:	MGlobal::displayWarning( str.CStr() );	return;
	case MT_ERROR:		MGlobal::displayError( str.CStr() );	return;
	default:			cout << str.CStr() << endl;				return;
	}
}
